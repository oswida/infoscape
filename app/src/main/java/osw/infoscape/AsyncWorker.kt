package osw.infoscape

import android.content.Context
import android.os.Handler
import android.os.HandlerThread

/**
 * Database worker. Thread handler to avoid calling database operations in GUI thread.
 */
class AsyncWorker(name: String) : HandlerThread(name) {

    private lateinit var mWorkerHandler: Handler

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        mWorkerHandler = Handler(looper)
    }

    fun postTask(task: Runnable) {
        if (mWorkerHandler != null) {
            mWorkerHandler.post(task)
        }
    }

    companion object {
        var INSTANCE: AsyncWorker? = null

        fun getInstance(): AsyncWorker? {
            if (INSTANCE == null) {
                INSTANCE = AsyncWorker("ASYNCWORKER")
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        fun postTask(context: Context?, func: () -> Unit) {
            getInstance()?.postTask(Runnable {
                func()
            })
        }
    }

}