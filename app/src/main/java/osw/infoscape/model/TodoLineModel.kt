package osw.infoscape.model

import android.app.Application
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.infoscape.R
import osw.infoscape.util.FileHelper
import osw.infoscape.util.TodoHelper
import java.text.SimpleDateFormat
import java.util.*

class TodoLineHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var txt: TextView? = null
    var prio: TextView? = null
    var icon: ImageView? = null
    var dateCreated: TextView? = null
    var dateFinished: TextView? = null
    var projects: TextView? = null
    var contexts: TextView? = null
//    var full: TextView? = null

    init {
        txt = itemView.findViewById(R.id.todoText)
        icon = itemView.findViewById(R.id.todoIcon)
        prio = itemView.findViewById(R.id.todoPrio)
        dateCreated = itemView.findViewById(R.id.todoDateCreated)
        dateFinished = itemView.findViewById(R.id.todoDateFinished)
        projects = itemView.findViewById(R.id.todoProjects)
        contexts = itemView.findViewById(R.id.todoContexts)
//        full = itemView.findViewById(R.id.todoFull)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }

    override fun getRearLeftView(): View {
        return itemView.findViewById(R.id.rear_left_view)
    }

    override fun getRearRightView(): View {
        return itemView.findViewById(R.id.rear_right_view)
    }
}

class TodoLineItem(val line: String) : AbstractFlexibleItem<FlexibleViewHolder>() {

    var completed: Boolean = false
    var priority: String = ""
    var text: String = ""
    var created: Date? = null
    var finished: Date? = null
    var projects: List<String> = listOf()
    var contexts: List<String> = listOf()
    var additional: Map<String, String> = mapOf()

    init {
        completed = TodoHelper.completed(line)
        priority = TodoHelper.priority(line)
        text = TodoHelper.text(line)
        created = TodoHelper.creationDate(line)
        finished = TodoHelper.completionDate(line)
        projects = TodoHelper.projects(line)
        contexts = TodoHelper.contexts(line)
        additional = TodoHelper.additional(line)
    }


    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        (holder as TodoLineHolder).txt?.text = text
        if (completed) {
            holder.icon?.setImageResource(R.drawable.ic_todo_completed)
        } else {
            holder.icon?.setImageResource(R.drawable.ic_todo_active)
        }
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val sb = StringBuilder()
        if (created != null) {
            holder.dateCreated?.text = "Created: " + sdf.format(created)
        } else {
            holder.dateCreated?.text = ""
        }
        if (finished != null) {
            holder.dateFinished?.text = "Completed: " + sdf.format(finished)
        } else {
            holder.dateFinished?.text = ""
        }
        holder.projects?.text = projects.joinToString(" ")
        holder.contexts?.text = contexts.joinToString(" ")
        holder.prio?.text = priority
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): TodoLineHolder {
        return TodoLineHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is TodoLineItem) {
            val inItem = other
            return hashCode() == inItem.hashCode()
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(line, completed, priority, text, created, finished, projects, contexts, additional)
    }

    override fun getLayoutRes(): Int {
        return R.layout.todo_line_item
    }

    override fun toString(): String {
        val sb = StringBuffer()
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        if (completed) {
            sb.append("x ")
        }
        if (!priority.isNullOrBlank() && !completed) {
            sb.append("(").append(priority).append(") ")
        }
        if (finished != null) {
            sb.append(sdf.format(finished)).append(" ")
        }
        if (created != null) {
            sb.append(sdf.format(created)).append(" ")
        }
        sb.append(text.trim()).append(" ")
        projects.forEach {
            sb.append("+").append(it).append(" ")
        }
        contexts.forEach {
            sb.append("@").append(it).append(" ")
        }
        additional.keys.forEach {
            sb.append(it).append(":").append(additional[it]).append(" ")
        }
        return sb.toString()
    }

}

class TodoLineModel(val app: Application) : AndroidViewModel(app) {

    var folder: String? = null
    var filename: String? = null

    var itemList: MutableLiveData<List<TodoLineItem>> = MutableLiveData()

    fun refresh(filter: String = "") {
        if (filename!= null) {
            val lines = FileHelper.readFile(
                app.applicationContext,
                filename!!
            ).lines()
            if (lines.isEmpty()) {
                return
            }
            itemList.postValue(lines
                .filter { it.trim().isNotEmpty() }
                .filter {
                    filter.isBlank() || it.contains(filter, true)
                }
                .map { TodoLineItem(it) }
                .sortedBy { if (it.priority.isNullOrBlank()) "Z" else it.priority })
        }
    }
}