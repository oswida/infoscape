package osw.infoscape.model

import android.app.Application
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.infoscape.R
import osw.infoscape.db.DbWorker
import osw.infoscape.db.SyncSite
import osw.infoscape.util.Consts
import osw.infoscape.util.FileHelper
import osw.infoscape.util.TextHelper
import java.io.File
import java.util.*

class FolderHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var title: TextView? = null
    var desc: TextView? = null
    var icon: ImageView? = null
    var menu: ImageView? = null

    init {
        title = itemView.findViewById(R.id.folderTitle)
        desc = itemView.findViewById(R.id.folderDesc)
        icon = itemView.findViewById(R.id.folderImage)
        menu = itemView.findViewById(R.id.itemmenu)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }


}

class FolderItem(val file: File, val site: SyncSite?,
                 val favourite: Boolean = false,
                 var menuClickFunc: ((FolderItem, View) -> Unit)? = null) :
    AbstractFlexibleItem<FlexibleViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        (holder as FolderHolder).title?.text = file.name
        if (file.isDirectory) {
            holder.icon?.setImageResource(R.drawable.ic_folder)
            if (file.listFiles() != null) {
                holder.desc?.text = file.listFiles().size.toString() + " items"
            } else {
                holder.desc?.text = "Empty"
            }
        } else {
            when (file.extension) {
                Consts.PDF_EXT -> {
                    holder.icon?.setImageResource(R.drawable.ic_pdf)
                }
                Consts.MARKDOWN_EXT -> {
                    holder.icon?.setImageResource(R.drawable.ic_org)
                }
                Consts.JPG_EXT, Consts.PNG_EXT, Consts.WEBP_EXT -> {
                    holder.icon?.setImageResource(R.drawable.ic_image)
                }
                Consts.TODO_EXT -> {
                    holder.icon?.setImageResource(R.drawable.ic_todo)
                }
                Consts.CSV_EXT -> {
                    holder.icon?.setImageResource(R.drawable.ic_csv)
                }
                else -> {

                }
            }
            holder.desc?.text = TextHelper.bytesText(file.length())
        }
        if (site != null) {
            holder.desc?.text = "⇅ " + site.title + "   " + holder.desc?.text.toString()
        }
        if (favourite) {
            holder.desc?.text = "★   " + holder.desc?.text.toString()
        }
        holder.menu!!.setOnClickListener {
            if (menuClickFunc != null) {
                menuClickFunc?.invoke(this, it)
            }
        }
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): FolderHolder {
        return FolderHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is FolderItem) {
            val inItem = other as File
            return file.absolutePath!! == inItem.absolutePath
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(file.absolutePath)
    }

    override fun getLayoutRes(): Int {
        return R.layout.folder_item
    }

}

class FolderModel(val app: Application) : AndroidViewModel(app) {

    var rootPath: String = ""

    var itemList: MutableLiveData<List<FolderItem>> = MutableLiveData()

    var menuClickFunc: ((FolderItem, View) -> Unit)? = null

    fun refresh(filter: String = "", filext: String = "") {
        DbWorker.postDbTask(app.applicationContext) { db ->
            val result = FileHelper.listFiles(app.applicationContext, rootPath)
            val value = result
                .filter { it.isDirectory || Consts.supportedFiles.contains(it.extension) }
                .filter { it.name.toUpperCase().startsWith(filter.toUpperCase()) }
                .filter {
                    if (filext.isNotBlank()) {
                        it.isDirectory || (it.isFile && it.extension == filext)
                    } else {
                        true
                    }
                }
                .map {
                    val path = FileHelper.relativePath(
                        app.applicationContext,
                        it.absolutePath
                    )
                    val f = db.favouriteDao().findForPath(path)
                    val fav = f != null
                    FolderItem(it, null, fav, menuClickFunc)
                }
            val part1 = value.filter { it.file.isDirectory }
                .sortedBy { it.file.name.toUpperCase() }
            val part2 = value.filter { it.file.isFile }
                .sortedBy { it.file.name.toUpperCase() }
            itemList.postValue(part1.plus(part2))
        }
    }
}