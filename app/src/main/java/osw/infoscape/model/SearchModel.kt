package osw.infoscape.model

import android.app.Application
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.infoscape.R
import osw.infoscape.util.Consts
import osw.infoscape.util.FileHelper
import osw.infoscape.util.LuceneHelper
import java.io.File
import java.util.*

class SearchHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var title: TextView? = null
    var icon: ImageView? = null

    init {
        title = itemView.findViewById(R.id.folderTitle)
        icon = itemView.findViewById(R.id.folderImage)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }

}

class SearchItem(val file: File) : AbstractFlexibleItem<FlexibleViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        (holder as SearchHolder).title?.text = file.name
        when (file.extension) {
            Consts.PDF_EXT -> {
                holder.icon?.setImageResource(R.drawable.ic_pdf)
            }
            Consts.MARKDOWN_EXT -> {
                holder.icon?.setImageResource(R.drawable.ic_org)
            }
            Consts.JPG_EXT, Consts.PNG_EXT, Consts.WEBP_EXT -> {
                holder.icon?.setImageResource(R.drawable.ic_image)
            }
            Consts.TODO_EXT -> {
                holder.icon?.setImageResource(R.drawable.ic_todo)
            }
            else -> {
                holder.icon?.setImageResource(R.drawable.ic_org)
            }
        }
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): SearchHolder {
        return SearchHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is SearchItem) {
            val inItem = other as File
            return file.absolutePath!! == inItem.absolutePath
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(file.absolutePath)
    }

    override fun getLayoutRes(): Int {
        return R.layout.search_item
    }

}

class SearchModel(val app: Application) : AndroidViewModel(app) {

    var itemList: MutableLiveData<List<SearchItem>> = MutableLiveData()

    fun refresh(filter: String = "") {
        val paths = LuceneHelper
            .findFilesByContent(app.applicationContext, filter, 30)
        itemList.postValue(paths
            .map { SearchItem(FileHelper.getFile(app.applicationContext, it)) })
    }
}