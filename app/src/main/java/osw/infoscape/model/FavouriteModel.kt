package osw.infoscape.model

import android.app.Application
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.infoscape.R
import osw.infoscape.db.DbWorker
import osw.infoscape.db.Favourite
import osw.infoscape.util.Consts
import osw.infoscape.util.FileHelper
import osw.infoscape.util.TextHelper
import java.util.*

class FavouriteHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var title: TextView? = null
    var desc: TextView? = null
    var icon: ImageView? = null
    var menu: ImageView? = null

    init {
        title = itemView.findViewById(R.id.folderTitle)
        desc = itemView.findViewById(R.id.folderDesc)
        icon = itemView.findViewById(R.id.folderImage)
        menu = itemView.findViewById(R.id.itemmenu)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }
}

class FavouriteItem(
    val favourite: Favourite,
    var menuClickFunc: ((FavouriteItem, View) -> Unit)? = null
) :
    AbstractFlexibleItem<FlexibleViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        var pth = favourite.path
        if (pth!!.startsWith("/")) {
            pth = pth.replaceFirst("/", "")
        }
        (holder as FavouriteHolder).title?.text = pth
        val file = FileHelper.getFile(holder.itemView.context, favourite.path!!)
        if (file.isDirectory) {
            holder.icon?.setImageResource(R.drawable.ic_folder)
            if (file.listFiles() != null) {
                holder.desc?.text = file.listFiles().size.toString() + " items"
            } else {
                holder.desc?.text = "Empty"
            }
        } else {
            when (file.extension) {
                Consts.PDF_EXT -> {
                    holder.icon?.setImageResource(R.drawable.ic_pdf)
                }
                Consts.MARKDOWN_EXT -> {
                    holder.icon?.setImageResource(R.drawable.ic_org)
                }
                Consts.JPG_EXT, Consts.PNG_EXT, Consts.WEBP_EXT -> {
                    holder.icon?.setImageResource(R.drawable.ic_image)
                }
                Consts.TODO_EXT -> {
                    holder.icon?.setImageResource(R.drawable.ic_todo)
                }
                else -> {
                }
            }
            holder.desc?.text = TextHelper.bytesText(file.length())
        }
        holder.menu!!.setOnClickListener {
            if (menuClickFunc != null) {
                menuClickFunc?.invoke(this, it)
            }
        }
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): FavouriteHolder {
        return FavouriteHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is FolderItem) {
            val inItem = other as Favourite
            return favourite.id!! == inItem.id
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(favourite.id)
    }

    override fun getLayoutRes(): Int {
        return R.layout.folder_item
    }

}

class FavouriteModel(val app: Application) : AndroidViewModel(app) {

    var itemList: MutableLiveData<List<FavouriteItem>> = MutableLiveData()

    var menuClickFunc: ((FavouriteItem, View) -> Unit)? = null

    fun refresh(filter: String = "") {
        DbWorker.postDbTask(app.applicationContext) { db ->
            var result: List<Favourite>
            if (filter.trim().isNotBlank()) {
                result = db.favouriteDao().getAll("%$filter%")
            } else {
                result = db.favouriteDao().getAll()
            }
            val value = result
                .map {
                    FavouriteItem(it, menuClickFunc)
                }
            itemList.postValue(value)
        }
    }
}