package osw.infoscape.db

import androidx.room.*
import androidx.room.ForeignKey.CASCADE

@Entity(tableName = "syncfile")
@ForeignKey(entity = SyncSite::class, parentColumns = ["id"], childColumns = ["siteId"],onDelete = CASCADE)
data class SyncFile(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "path") var path: String?,
    @ColumnInfo(name = "siteId") var siteId: Long)

@Dao
interface SyncFileDao {

    @Query("SELECT * FROM syncfile WHERE siteId=:siteId")
    fun getAll(siteId: Long): List<SyncFile>

    @Query("SELECT * FROM syncfile WHERE path=:path LIMIT 1")
    fun findByPath(path: String?): SyncFile?

    @Insert
    fun insertAll(vararg items: SyncFile)

    @Delete
    fun delete(item: SyncFile)

    @Query("DELETE FROM syncfile WHERE siteId=:siteId")
    fun clear(siteId: Long)

    @Update
    fun save(item: SyncFile)
}
