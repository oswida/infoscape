package osw.infoscape.db

import androidx.room.*

@Entity(tableName = "favourite")
data class Favourite(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "path") var path: String?,
    @ColumnInfo(name = "sticky") var sticky: Boolean = false
)

@Dao
interface FavouriteDao {

    @Query("SELECT * FROM favourite WHERE path LIKE :filter")
    fun getAll(filter: String): List<Favourite>

    @Query("SELECT * FROM favourite")
    fun getAll(): List<Favourite>

    @Query("SELECT * FROM favourite WHERE path=:path")
    fun findForPath(path: String): Favourite

    @Insert
    fun insertAll(vararg items: Favourite)

    @Delete
    fun delete(item: Favourite)

    @Query("DELETE FROM favourite WHERE path=:path")
    fun deleteByPath(path: String)

    @Query("DELETE FROM favourite")
    fun clear()

    @Update
    fun save(item: Favourite)
}

