package osw.infoscape.db

import androidx.room.*


@Entity(tableName = "lastpos")
data class LastPos(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "path") var path: String?,
    @ColumnInfo(name = "pos") var pos: Long?
)

@Dao
interface LastPosDao {

    @Query("SELECT * FROM lastpos")
    fun getAll(): List<LastPos>

    @Query("SELECT * FROM lastpos WHERE path=:path")
    fun findForPath(path: String): LastPos

    @Insert
    fun insertAll(vararg items: LastPos)

    @Delete
    fun delete(item: LastPos)

    @Query("DELETE FROM lastpos WHERE path=:path")
    fun deleteForPath(path: String)

    @Query("DELETE FROM lastpos WHERE id=:id")
    fun deleteById(id: Long)

    @Query("DELETE FROM lastpos")
    fun clear()

    @Update
    fun save(item: LastPos)
}