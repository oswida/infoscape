package osw.infoscape.db

import androidx.room.*

@Entity(tableName = "pdfnote")
data class PdfNote(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "path") var path: String?,
    @ColumnInfo(name = "page") var page: Int?,
    @ColumnInfo(name = "content") var content: String?
)

@Dao
interface PdfNoteDao {

    @Query("SELECT * FROM pdfnote")
    fun getAll(): List<PdfNote>

    @Query("SELECT * FROM pdfnote WHERE path=:path")
    fun getAll(path: String): List<PdfNote>

    @Query("SELECT count(*) FROM pdfnote WHERE path=:path AND page=:page")
    fun findForPage(path: String, page: Int): Int

    @Insert
    fun insertAll(vararg items: PdfNote)

    @Delete
    fun delete(item: PdfNote)

    @Query("DELETE FROM pdfnote WHERE path=:path")
    fun deleteForPath(path: String)

    @Query("DELETE FROM pdfnote WHERE id=:id")
    fun deleteById(id: Long)

    @Query("DELETE FROM pdfnote")
    fun clear()

    @Update
    fun save(item: PdfNote)
}