package osw.infoscape.db

import androidx.room.*

@Entity(tableName = "syncsite")
data class SyncSite(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "title") var title: String?,
    @ColumnInfo(name = "type") var type: String?,
    @ColumnInfo(name = "data") var data: String?
)

@Dao
interface SyncSiteDao {

    @Query("SELECT * FROM syncsite")
    fun getAll(): List<SyncSite>

    @Query("SELECT * FROM syncsite WHERE title LIKE :filter")
    fun getAll(filter: String): List<SyncSite>

    @Query("SELECT * FROM syncsite WHERE title=:title")
    fun findByTitle(title: String?): SyncSite?

    @Query("SELECT * FROM syncsite WHERE id=:id")
    fun findById(id: Long?): SyncSite?

    @Insert
    fun insertAll(vararg items: SyncSite)

    @Delete
    fun delete(item: SyncSite)

    @Query("DELETE FROM syncsite")
    fun clear()

    @Update
    fun save(item: SyncSite)
}