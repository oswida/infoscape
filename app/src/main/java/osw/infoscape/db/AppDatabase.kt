package osw.infoscape.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * Application database.
 */
@Database(
    entities = [
        Favourite::class,
        PdfBookmark::class,
        PdfNote::class,
        LastPos::class
    ],
    version = 4
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun pdfBookmarkDao(): PdfBookmarkDao
    abstract fun favouriteDao(): FavouriteDao
    abstract fun pdfNoteDao(): PdfNoteDao
    abstract fun lastPosDao(): LastPosDao

    companion object {
        var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java, "infoscape.db"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }
}


