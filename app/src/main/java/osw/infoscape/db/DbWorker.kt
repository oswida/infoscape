package osw.infoscape.db

import android.content.Context
import android.os.Handler
import android.os.HandlerThread

/**
 * Database worker. Thread handler to avoid calling database operations in GUI thread.
 */
class DbWorker(name: String) : HandlerThread(name) {

    private lateinit var mWorkerHandler: Handler

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        mWorkerHandler = Handler(looper)
    }

    fun postTask(task: Runnable) {
        if (mWorkerHandler != null) {
            mWorkerHandler.post(task)
        }
    }

    companion object {
        var INSTANCE: DbWorker? = null

        fun getInstance(): DbWorker? {
            if (INSTANCE == null) {
                INSTANCE = DbWorker("DBWORKER")
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        fun postDbTask(context: Context?, func: (AppDatabase) -> Unit) {
            getInstance()?.postTask(Runnable {
                val db = AppDatabase.getInstance(context!!)
                if (db != null) {
                    func(db)
                }
            })
        }
    }

}