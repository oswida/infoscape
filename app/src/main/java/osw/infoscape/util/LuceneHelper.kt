package osw.infoscape.util

import android.content.Context
import android.util.Log
import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.document.Document
import org.apache.lucene.document.Field
import org.apache.lucene.document.StringField
import org.apache.lucene.document.TextField
import org.apache.lucene.index.DirectoryReader
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.IndexWriterConfig
import org.apache.lucene.index.Term
import org.apache.lucene.queryparser.classic.QueryParser
import org.apache.lucene.search.IndexSearcher
import org.apache.lucene.store.FSDirectory
import org.apache.lucene.util.Version
import java.io.File
import java.io.IOException


object LuceneHelper {

    private val LOG_TAG = "LuceneHelper"

    val FLD_PATH = "path"
    val FLD_CONTENT = "content"
    val FLD_NAME = "name"

    private var writer: IndexWriter? = null
    private var analyzer: Analyzer? = null
    private var iwc: IndexWriterConfig? = null
    private var indexDir: FSDirectory? = null


    @Throws(IOException::class)
    fun init(context: Context) {
        val dir = FileHelper.getFile(context, Consts.LUCENE_DIR)
        if (!dir.exists()) {
            dir.mkdirs()
        }
        indexDir = FSDirectory.open(dir)
        analyzer = StandardAnalyzer(Version.LUCENE_41)
        iwc = IndexWriterConfig(Version.LUCENE_41, analyzer)
        iwc?.openMode = IndexWriterConfig.OpenMode.CREATE_OR_APPEND
        iwc?.ramBufferSizeMB = 16.0
        try {
            writer = IndexWriter(indexDir, iwc)
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.message, e)
        }
    }

    @Throws(IOException::class)
    fun close() {
        writer!!.close()
    }

    fun indexFile(context: Context, path: String) {
        val contents = FileHelper.readFile(context, path)
        val doc = Document()
        val pathField = StringField(FLD_PATH, path, Field.Store.YES)
        val file = FileHelper.getFile(context, path)
        val nameField = StringField(FLD_NAME, file.nameWithoutExtension.toLowerCase(), Field.Store.YES)
        val contentField = TextField(FLD_CONTENT, contents, Field.Store.NO)
        doc.add(pathField)
        doc.add(contentField)
        doc.add(nameField)
        writer?.deleteDocuments(Term(FLD_PATH, path))
        writer?.addDocument(doc)
        writer?.commit()
    }

    fun indexFileTitle(context: Context, path: String) {
        val doc = Document()
        val pathField = StringField(FLD_PATH, path, Field.Store.YES)
        val file = FileHelper.getFile(context, path)
        val nameField = StringField(FLD_NAME, file.nameWithoutExtension.toLowerCase(), Field.Store.YES)
        doc.add(pathField)
        doc.add(nameField)
        writer?.deleteDocuments(Term(FLD_PATH, path))
        writer?.addDocument(doc)
        writer?.commit()
    }

    fun recreateIndex(context: Context) {
        val list = mutableListOf<File>()
        FileHelper.getAllFiles(context, list)
        writer!!.deleteAll()
        list.forEach {
            when (it.extension) {
                Consts.TODO_EXT, Consts.MARKDOWN_EXT, Consts.CSV_EXT -> indexFile(
                    context,
                    FileHelper.relativePath(context, it.absolutePath)
                )
                Consts.PNG_EXT, Consts.JPG_EXT, Consts.PDF_EXT -> indexFileTitle(
                    context,
                    FileHelper.relativePath(context, it.absolutePath)
                )
            }
        }
    }

    fun removeDocument(context: Context, path: String) {
        writer?.deleteDocuments(Term(FLD_PATH, path))
    }

    fun findFilesByContent(context: Context, term: String, maxResults: Int): List<String> {
        if (term.isNullOrBlank()) {
            return listOf()
        }
        try {
            val reader = DirectoryReader.open(indexDir)
            val searcher = IndexSearcher(reader)
            val parser = QueryParser(Version.LUCENE_41, FLD_CONTENT, analyzer)
            val query = parser.parse(term)
            val hits = searcher?.search(query, maxResults)
            val result = mutableListOf<String>()
            hits?.scoreDocs?.sortedBy { it.score }!!.forEach {
                val document = reader?.document(it.doc)
                result.add(document!!.get(FLD_PATH))
            }
            return result
        } catch (ex: Exception) {
            Log.i("LUCENE", ex.toString())
        }
        return listOf()
    }

    fun findFilesByField(context: Context, term: String, field: String, maxResults: Int): List<String> {
        if (term.isNullOrBlank()) {
            return listOf()
        }
        try {
            val reader = DirectoryReader.open(indexDir)
            val searcher = IndexSearcher(reader)
            val parser = QueryParser(Version.LUCENE_41, field, analyzer)
            val query = parser.parse(term)
            val hits = searcher?.search(query, maxResults)
            val result = mutableListOf<String>()
            hits?.scoreDocs?.sortedBy { it.score }!!.forEach {
                val document = reader?.document(it.doc)
                result.add(document!!.get(FLD_PATH))
            }
            return result
        } catch (ex: Exception) {
            Log.i("LUCENE", ex.toString())
        }
        return listOf()
    }


}