package osw.infoscape.util

import android.content.Context
import android.graphics.Color
import android.text.InputType
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.*
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.color.colorChooser
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.files.FileFilter
import com.afollestad.materialdialogs.files.fileChooser
import com.afollestad.materialdialogs.files.folderChooser
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.list.listItems
import java.io.File

object DialogHelper {

    fun input(
        context: Context?, title: String, message: String,
        defaultVal: String, func: (text: String?) -> Unit
    ) {
        val dlg = MaterialDialog(context!!)
        dlg.title(text = title)
        dlg.message(text = message)
        dlg.input(prefill = defaultVal) { _, charSequence ->
            func(charSequence.toString())
        }.negativeButton(text = "Cancel")
            .show()
    }

    fun inputPass(
        context: Context?, title: String, message: String,
        func: (text: String?) -> Unit
    ) {
        val dlg = MaterialDialog(context!!)
        dlg.title(text = title)
        dlg.message(text = message)
        dlg.input(inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
        { _, charSequence ->
            func(charSequence.toString())
        }.negativeButton(text = "Cancel")
            .show()
    }

    fun inputInt(
        context: Context?, title: String, message: String,
        defaultVal: Int, func: (Int?) -> Unit
    ) {
        val dlg = MaterialDialog(context!!)
        dlg.title(text = title)
        dlg.message(text = message)
        dlg.input(prefill = defaultVal.toString(), inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED) { _, charSequence ->
            func(charSequence.toString().toIntOrNull())
        }.negativeButton(text = "Cancel")
        dlg.show()
    }

    fun selectInt(
        context: Context?, title: String, message: String,
        defaultVal: Int, minVal: Int, maxVal: Int, func: (Int?) -> Unit
    ) {
        val picker = NumberPicker(context!!)
        picker.minValue = minVal
        picker.maxValue = maxVal
        picker.value = defaultVal
        val dlg = MaterialDialog(context)
        dlg.title(text = title)
            .customView(view = picker)
            .positiveButton(text = "Ok") {
                func(picker.value)
            }.negativeButton(text = "Cancel")
        dlg.show()
    }

    fun inputFloat(
        context: Context?, title: String, message: String,
        defaultVal: Float, func: (text: Float?) -> Unit
    ) {
        val dlg = MaterialDialog(context!!)
        val tp = InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED
        dlg.title(text = title)
        dlg.message(text = message)
        dlg.input(prefill = defaultVal.toString(), inputType = tp) { _, charSequence ->
            func(charSequence.toString().toFloatOrNull())
        }.negativeButton(text = "Cancel")
            .show()
    }

    fun confirm(
        context: Context?, title: String, message: String,
        func: () -> Unit, cancelfunc: (() -> Unit)? = null
    ) {
        MaterialDialog(context!!)
            .title(text = title)
            .message(text = message)
            .positiveButton(text = "Ok") {
                func()
            }
            .negativeButton(text = "Cancel") {
                if (cancelfunc != null) {
                    cancelfunc()
                }
            }
            .show()
    }

    fun select(context: Context?, title: String, items: List<String>, func: (Int, String) -> Unit) {
        if (title.isBlank()) {
            MaterialDialog(context!!)
                .listItems(items = items) { _, index, text ->
                    func(index, text)
                }
                .show()
        } else {
            MaterialDialog(context!!)
                .title(text = title)
                .listItems(items = items) { _, index, text ->
                    func(index, text)
                }
                .show()
        }
    }

    fun autocomplete(context: Context?, title: String, items: List<String>, func: (String) -> Unit) {
        val layout = LinearLayout(context)
        layout.setPadding(5, 5, 5, 5)
        val aview = AutoCompleteTextView(context)
        aview.setAdapter(
            ArrayAdapter<String>(
                context,
                android.R.layout.simple_list_item_1, items.toMutableList()
            )
        )
        layout.addView(aview)
        aview.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        aview.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        MaterialDialog(context!!)
            .title(text = title)
            .customView(view = layout)
            .positiveButton {
                if (!aview.text.toString().isBlank()) {
                    func(aview.text.toString())
                }
            }
            .show()
    }

//    fun chipAutocomplete(
//        context: Context?, title: String,
//        items: List<String>,
//        defaultVal: String,
//        func: (List<String>) -> Unit
//    ) {
//        val layout = LinearLayout(context)
//        layout.setPadding(5, 5, 5, 5)
//        val aview = NachoTextView(context)
//        aview.setAdapter(
//            ArrayAdapter<String>(
//                context,
//                android.R.layout.simple_list_item_1, items.toMutableList()
//            )
//        )
//        layout.addView(aview)
//        aview.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
//        aview.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
//        aview.addChipTerminator('\n', ChipTerminatorHandler.BEHAVIOR_CHIPIFY_TO_TERMINATOR)
//        aview.addChipTerminator(' ', ChipTerminatorHandler.BEHAVIOR_CHIPIFY_TO_TERMINATOR)
//        aview.setPasteBehavior(ChipTerminatorHandler.BEHAVIOR_CHIPIFY_ALL)
//        aview.chipSpacing = R.dimen.md_action_button_spacing
//        aview.setText("$defaultVal ")
//        MaterialDialog(context!!)
//            .title(text = title)
//            .customView(view = layout)
//            .positiveButton {
//                if (!aview.text.toString().isBlank()) {
//                    func(aview.chipValues)
//                }
//            }
//            .show()
//    }

    fun doubleinput(
        context: Context?, title: String, msgOne: String,
        msgTwo: String, defOne: String, defTwo: String, func: (String, String) -> Unit
    ) {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.setPadding(5, 5, 5, 5)
        val editOne = EditText(context)
        editOne.setText(defOne)
        val labelOne = TextView(context)
        labelOne.text = msgOne
        val editTwo = EditText(context)
        editTwo.setText(defTwo)
        val labelTwo = TextView(context)
        labelTwo.text = msgTwo
        layout.addView(labelOne)
        layout.addView(editOne)
        layout.addView(labelTwo)
        layout.addView(editTwo)
//        aview.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
//        aview.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        MaterialDialog(context!!)
            .title(text = title)
            .customView(view = layout)
            .positiveButton {
                func(editOne.text.toString(), editTwo.text.toString())
            }
            .negativeButton { }
            .show()
    }


    fun info(context: Context?, infoTitle: String, infoMessage: String) {
        MaterialDialog(context!!).show {
            title(text = infoTitle)
            message(text = infoMessage)
        }
    }

    fun selectFile(context: Context?, initalDir: File, filter: FileFilter = null, func: (File) -> Unit) {
        if (!initalDir.exists()) {
            initalDir.mkdirs()
        }
        MaterialDialog(context!!)
            .negativeButton(text = "Cancel")
            .fileChooser(initalDir, filter = filter) { _, file ->
                func(file)
            }.show()
    }

    fun selectFolder(context: Context?, initalDir: File, filter: FileFilter = null, func: (File) -> Unit) {
        MaterialDialog(context!!)
            .negativeButton(text = "Cancel")
            .folderChooser(initalDir, filter = filter) { _, file ->
                func(file)
            }.show()
    }

    fun selectColor(context: Context?, title: String, selColor: Int, func: (Int) -> Unit) {
        val colors = intArrayOf(
            Color.BLACK, Color.BLUE, Color.CYAN, Color.DKGRAY, Color.GRAY, Color.GREEN,
            Color.LTGRAY, Color.MAGENTA, Color.RED, Color.WHITE, Color.YELLOW
        )
        MaterialDialog(context!!)
            .title(text = title)
            .colorChooser(colors = colors, initialSelection = selColor) { _, color ->
                func(color)
            }
            .show()
    }

    fun webinfo(context: Context?, title: String, data: String) {
        val layout = LinearLayout(context)
        layout.setPadding(5, 5, 5, 5)
        val webview = WebView(context)
        webview.loadDataWithBaseURL("", data, "text/html", "UTF-8", "")
        layout.addView(webview)
        MaterialDialog(context!!)
            .customView(view = layout, scrollable = true)
            .title(text = title)
            .show()
    }


    fun nextcloudData(
        context: Context?, title: String,
        func: (String, String, String, String) -> Unit
    ) {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.setPadding(5, 5, 5, 5)
        val editOne = EditText(context)
        val labelOne = TextView(context)
        labelOne.text = "Site URL"
        val editTwo = EditText(context)
        val labelTwo = TextView(context)
        labelTwo.text = "Username"
        val editThree = EditText(context)
        editThree.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        val labelThree = TextView(context)
        labelThree.text = "Password"
        val editFour = EditText(context)
        val labelFour = TextView(context)
        labelFour.text = "Folder"
        layout.addView(labelOne)
        layout.addView(editOne)
        layout.addView(labelTwo)
        layout.addView(editTwo)
        layout.addView(labelThree)
        layout.addView(editThree)
        layout.addView(labelFour)
        layout.addView(editFour)
        MaterialDialog(context!!)
            .title(text = title)
            .customView(view = layout)
            .positiveButton {
                func(
                    editOne.text.toString(), editTwo.text.toString(),
                    editThree.text.toString(), editFour.text.toString()
                )
            }
            .negativeButton { }
            .show()
    }


}