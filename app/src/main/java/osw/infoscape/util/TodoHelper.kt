package osw.infoscape.util

import android.content.Context
import android.util.Log
import br.tiagohm.markdownview.css.styles.Github
import osw.infoscape.theme.GithubDark
import java.text.SimpleDateFormat
import java.util.*

object TodoHelper {

    const val EXAMPLE_TODO = ""

    const val SORT_TEXT = 0
    const val SORT_CREATION_DATE = 1
    const val SORT_PRIO = 2

    const val PT_DATE = "\\d{4}-\\d{2}-\\d{2}"
    val PATTERN_PROJECTS = Regex("\\B(?:\\++)(\\w+)")
    val PATTERN_CONTEXTS = Regex("\\B(?:\\@+)(\\w+)")
    val PATTERN_DONE = Regex("(?m)(^[Xx]) (.*)$")
    val PATTERN_PRIORITY_ANY = Regex("(?:^|\\n)\\(([A-Za-z])\\)\\s")
    val PATTERN_DATE = Regex("(?:^|\\s|:)($PT_DATE)(?:$|\\s)")
    val PATTERN_KEY_VALUE_PAIRS = Regex("(?i)((?:[a-z]+):(?:[a-z0-9_-]+))")
    val PATTERN_COMPLETION_DATE = Regex("(?:^|\\n)(?:[Xx] )($PT_DATE)")
    val PATTERN_CREATION_DATE = Regex("(?:^|\\n)(?:\\([A-Za-z]\\)\\s)?(?:[Xx] $PT_DATE )?($PT_DATE)")

//    val PATTERN_KEY_VALUE_PAIRS__TAG_ONLY = Regex("(?i)([a-z]+):([a-z0-9_-]+)")
//    val PATTERN_PRIORITY_A = Regex("(?:^|\\n)\\(([Aa])\\)\\s")
//    val PATTERN_PRIORITY_B = Regex("(?:^|\\n)\\(([Bb])\\)\\s")
//    val PATTERN_PRIORITY_C = Regex("(?:^|\\n)\\(([Cc])\\)\\s")
//    val PATTERN_PRIORITY_D = Regex("(?:^|\\n)\\(([Dd])\\)\\s")
//    val PATTERN_PRIORITY_E = Regex("(?:^|\\n)\\(([Ee])\\)\\s")
//    val PATTERN_PRIORITY_F = Regex("(?:^|\\n)\\(([Ff])\\)\\s")


    private fun currentDate(): String {
        return SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())
    }

    fun update(context: Context, filename: String, original: String, line: String) {
        val lines = FileHelper.readFile(context, filename).lines()
        val result = lines.map {
            if (it.trim() == original.trim()) {
                line
            } else {
                it
            }
        }.filter { it.trim().isNotBlank() }
        Log.i("UPDATING", line)
        FileHelper.saveFile(context, filename, result.joinToString("\n"))
    }

    fun toggleDone(context: Context, filename: String, line: String) {
        var toUpdate = line.trim()
        if (line.startsWith("x ")) {
            toUpdate = toUpdate.replaceFirst("x ", "").trim()
            val match = Regex(PT_DATE).findAll(toUpdate)
            if (match.count() == 2) {
                toUpdate = toUpdate.replaceRange(match.elementAt(1).range, "")
            }
        } else {
            val match = Regex(PT_DATE).findAll(toUpdate)
            when (match.count()) {
                0 -> {
                    toUpdate = currentDate() + " $toUpdate"
                }
                1 -> {
                    val el = match.elementAt(0)
                    toUpdate = toUpdate.replaceRange(
                        el.range,
                        el.value + " " + currentDate()
                    )
                }
            }
            toUpdate = "x $toUpdate"
        }
        update(context, filename, line, toUpdate)
    }

    fun insert(context: Context, filename: String, line: String) {
        var toAdd = line
        val match = PATTERN_PRIORITY_ANY.find(toAdd)
        if (match != null) {
            val start = toAdd.indexOf("(")
            val end = toAdd.indexOf(")")
            val data = toAdd.substring(start, end)
            toAdd = toAdd.replaceRange(
                start,
                end,
                data + " " + currentDate() + " "
            )
        } else {
            toAdd = currentDate() + " " + toAdd
        }
        val lines = FileHelper.readFile(context, filename).lines()
        val result = mutableListOf<String>()
        result.addAll(lines.filter { it.trim().isNotBlank() })
        result.add(toAdd)
        FileHelper.saveFile(context, filename, result.joinToString("\n"))
    }

    fun delete(context: Context, filename: String, line: String) {
        val lines = FileHelper.readFile(context, filename).lines()
        val result = lines.filter {
            it.trim() != line.trim()
        }
        FileHelper.saveFile(context, filename, result.joinToString("\n"))
    }

    fun setPrio(context: Context, filename: String, line: String, prio: String) {
        var toUpdate = line.trim()
        val match = PATTERN_PRIORITY_ANY.find(toUpdate)
        if (match != null) {
            toUpdate = toUpdate.replaceRange(match.groups[1]!!.range, prio)
        } else {
            if (!toUpdate.startsWith("x")) {
                toUpdate = "($prio) $toUpdate"
            }
        }
        toUpdate = toUpdate.replaceFirst("()", "")
        update(context, filename, line, toUpdate)
    }

    fun archive(context: Context, filename: String, line: String) {
        val lines = FileHelper.readFile(context, filename).lines()
        val result = lines.filter { it.trim() != line.trim() }
        FileHelper.saveFile(context, filename, result.joinToString("\n"))
        val donefile = filename.replace(".${Consts.TODO_EXT}", ".${Consts.TODO_ARCHIVE_EXT}")
        val dones = FileHelper.readFile(
            context,
            donefile
        ).lines()
        val dr = mutableListOf<String>()
        dr.add(line)
        dr.addAll(dones)
        FileHelper.saveFile(
            context,
            donefile,
            dr.joinToString("\n")
        )
    }

    fun archiveAll(context: Context, filename: String) {
        val lines = FileHelper.readFile(context, filename).lines()

        val result = lines.filter { !it.trim().startsWith("x") }
        FileHelper.saveFile(context, filename, result.joinToString("\n"))
        val donefile = filename.replace(".${Consts.TODO_EXT}", ".${Consts.TODO_ARCHIVE_EXT}")
        val dones = FileHelper.readFile(
            context,
            donefile
        ).lines()
        val dr = mutableListOf<String>()
        dr.addAll(lines.filter { it.trim().startsWith("x") })
        dr.addAll(dones)
        FileHelper.saveFile(
            context,
            donefile,
            dr.joinToString("\n")
        )
    }


    fun toHtml(context: Context, text: String, archive: Boolean = false): String {
        var style = Github()
        if (PrefHelper.isNightmode(context)) {
            style = GithubDark(context)
            style.addRule(
                ".itm", "padding: 10px", "margin-top: 10px", "border-left: solid 3px orange",
                "border-right: solid 3px orange", "width: 100%", "vertical-align: middle"
            )
            style.addRule(".project", "color: orange")
        } else {
            style.addRule(
                ".itm", "padding: 10px", "margin-top: 10px", "border-left: solid 3px brown",
                "border-right: solid 3px brown", "width: 100%", "vertical-align: middle"
            )
            style.addRule(".project", "color: brown")
        }
        style.addRule(".date", "color: grey")
        style.addRule("body", "margin: 5px", "padding: 5px")
        if (!archive) {
            style.addRule(".completed", "text-decoration: line-through")
        }
        style.addRule(".right", "float: right")
        style.addRule(
            "body",
            "font-size: " +
                    SysUtils.dipToPix(context!!, PrefHelper.getWebViewFont(context!!).toFloat()) + "px"
        )
        var result = text
        result = PATTERN_DONE.replace(result, "<span class=\"completed\">$0</span>")
        result = PATTERN_PROJECTS.replace(result, "<strong class=\"project\">$0</strong>")
        result = PATTERN_CONTEXTS.replace(result, "<em class=\"project\">$0</em>")
        result = PATTERN_PRIORITY_ANY.replace(result, "\n<code>$1</code> ")
        if (result.startsWith("\n")) {
            result = result.replaceFirst("\n", "")
        }
        result = Regex(PT_DATE).replace(result, "<span class=\"date\">$0</span>")
        val lines = result.split("\n")
        var no = 0
        result = ""
        lines.forEach {
            if (it.trim().isNotBlank()) {
                result += "<div onclick=\"Android.showMenu($no)\" class=\"itm\">$it</div>"
            }
            no++
        }
        return "<html><head>" + style.toHTML() + "</head><body>$result</body></html>"
    }

    fun sort(context: Context, filename: String, sort: Int = SORT_TEXT, reverse: Boolean = false) {
        val lines = FileHelper.readFile(context, filename).lines()
        var result = lines.sortedBy {
            when (sort) {
                SORT_PRIO -> {
                    val match = PATTERN_PRIORITY_ANY.find(it)
                    if (match != null) {
                        match.value
                    } else {
                        ""
                    }
                }
                SORT_CREATION_DATE -> {
                    val match = PATTERN_DATE.find(it)
                    if (match != null) {
                        match.value
                    } else {
                        ""
                    }
                }
                else -> {
                    var res = PATTERN_PRIORITY_ANY.replace(it, "")
                    res = res.replace("()", "")
                    res = PATTERN_DATE.replace(res, "")
                    res.trim()
                }
            }
        }
        if (reverse) {
            result = result.reversed()
        }
        FileHelper.saveFile(context, filename, result.joinToString("\n"))
    }

    fun completed(line: String): Boolean {
        return line.startsWith("x ")
    }

    fun priority(line: String): String {
        val match = PATTERN_PRIORITY_ANY.find(line)
        if (match != null) {
            return match.groups[1]!!.value
        }
        return ""
    }

    fun text(line: String): String {
        var result = line
        if (result.trim().startsWith("x ")) {
            result = result.trim().substring(2)
        }
        var match = PATTERN_PRIORITY_ANY.find(line)
        if (match != null) {
            result = result.replaceRange(match.groups[1]!!.range, "")
            result = result.replace("()", "")
        }
        result = Regex(PT_DATE).replace(result, "")
        result = PATTERN_CONTEXTS.replace(result, "")
        result = PATTERN_PROJECTS.replace(result, "")
        return result.trim()
    }

    fun creationDate(line: String): Date? {
        val match = PATTERN_CREATION_DATE.find(line)
        if (match != null) {
            return return SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(match.groups[1]!!.value)
        }
        return null
    }

    fun completionDate(line: String): Date? {
        val match = PATTERN_COMPLETION_DATE.find(line)
        if (match != null) {
            return return SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(match.groups[1]!!.value)
        }
        return null
    }

    fun projects(line: String): List<String> {
        val result = mutableListOf<String>()
        val matches = PATTERN_PROJECTS.findAll(line)
        matches.forEach {
            result.add(it.groups[1]!!.value)
        }
        return result
    }

    fun contexts(line: String): List<String> {
        val result = mutableListOf<String>()
        val matches = PATTERN_CONTEXTS.findAll(line)
        matches.forEach {
            result.add(it.groups[1]!!.value)
        }
        return result
    }

    fun additional(line: String): Map<String, String> {
        val result = mutableMapOf<String, String>()
        val matches = PATTERN_KEY_VALUE_PAIRS.findAll(line)
        matches.forEach {
            val key = it.groups[1]!!.value
            val vv = it.groups[2]!!.value
            result[key] = vv
        }
        return result
    }

}