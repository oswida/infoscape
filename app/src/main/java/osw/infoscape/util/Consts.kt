package osw.infoscape.util

object Consts {
    const val SWIPE_LEFT = 4
    const val SWIPE_RIGHT = 8

    const val SYS_DIR = "sys"
    const val LUCENE_DIR = "$SYS_DIR/lucene"
    const val EXPORT_DIR = "$SYS_DIR/export"

    const val MARKDOWN_EXT = "md"
    const val TEXT_EXT = "txt"
    const val PDF_EXT = "pdf"
    const val JPG_EXT = "jpg"
    const val WEBP_EXT = "webp"
    const val PNG_EXT = "png"
    const val TODO_EXT = "todo"
    const val TODO_ARCHIVE_EXT = "done"
    const val CSV_EXT = "csv"

    val supportedFiles = listOf(TEXT_EXT, PDF_EXT,
        JPG_EXT, PNG_EXT, MARKDOWN_EXT, TODO_EXT,
        CSV_EXT, WEBP_EXT)

    const val TEST_CONTENT = ""

    const val INTERNAL_FILE_SCHEME = "doc://"
    const val INTERNAL_EDIT_TEXT_SCHEME = "edittext://"
    const val INTERNAL_EDIT_INT_SCHEME = "editint://"
    const val INTERNAL_EDIT_FLOAT_SCHEME = "editfloat://"
    const val INTERNAL_EDIT_ONOFF_SCHEME = "editonoff://"
    const val INTERNAL_HIDDEN_SCHEME = "hidden://"
    const val EXTERNAL_FILE_SCHEME = "file://"

    const val EDITABLE_TEXT_MARK = "!([^\\[][^!]+)!"
    const val EDITABLE_INT_MARK = "%(-?[0-9]+)%"
    const val EDITABLE_FLOAT_MARK = "&(-?[0-9\\.,]+)&"
    const val EDITABLE_ONOFF_MARK = "\\?([^\\n\\|?]+)\\|([^?\\n]+)\\?"

    const val HIDDEN_MARK = "(?:[^|\\n])@([^\\n\\|@]+)\\|([^@]+)@"

}