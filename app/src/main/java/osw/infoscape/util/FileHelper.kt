package osw.infoscape.util

import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.provider.OpenableColumns
import java.io.*
import java.sql.Timestamp


object FileHelper {

    val filenames = mutableListOf<File>()

    private val specialDirs = listOf(Consts.LUCENE_DIR, Consts.EXPORT_DIR, Consts.SYS_DIR)

    fun initFilenames(context: Context?) {
        filenames.clear()
        getAllFiles(context!!, filenames, "")
    }

    fun getDir(context: Context?, path: String): String {
        return PrefHelper.getRootDir(context!!) + File.separator + path
    }

    fun getFile(context: Context?, path: String): File {
        var parts = path.split(File.separator)
        val filename = parts.last()
        parts = parts.dropLast(1)
        val dir = parts.joinToString(File.separator)
        val target = Environment.getExternalStoragePublicDirectory(getDir(context, dir))
        if (!target.exists()) {
            target.mkdirs()
        }
        return target.resolve(filename)
    }

    fun listFiles(context: Context?, path: String): List<File> {
        val target = Environment.getExternalStoragePublicDirectory(getDir(context, path))
        if (target != null) {
            if (!target.exists()) {
                target.mkdirs()
            }
            return target.listFiles()!!.toList().filter {
                !specialDirs.contains(it.name.toLowerCase())
            }
        }
        return listOf()
    }

    fun createFolder(context: Context?, path: String) {
        if (specialDirs.contains(path)) {
            return
        }
        val target = Environment.getExternalStoragePublicDirectory(getDir(context, path))
        if (!target.exists()) {
            target.mkdirs()
        }
    }

    fun createFile(context: Context?, path: String) {
        val target = getFile(context, path)
        if (!target.exists()) {
            target.createNewFile()
        }
        initFilenames(context!!)
    }

    fun deleteFile(context: Context?, path: String) {
        val target = getFile(context, path)
        if (target.exists()) {
            target.delete()
        }
        initFilenames(context!!)
    }

    fun renameFile(context: Context?, oldpath: String, newpath: String) {
        val target = getFile(context, oldpath)
        if (!target.exists()) {
            return
        }
        target.renameTo(getFile(context, newpath))
        initFilenames(context!!)
    }

    fun copyFile(context: Context?, sourceuri: Uri, destpath: String) {
        val target = getFile(context, destpath)
        if (!target.exists()) {
            target.createNewFile()
        }
        var bis: InputStream? = null
        var bos: OutputStream? = null

        try {
            bis = context?.contentResolver!!.openInputStream(sourceuri)
            bos = FileOutputStream(target, false)
            val buf = bis.readBytes()
            bos.write(buf)
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                bis?.close()
                bos?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        when (target.extension) {
            Consts.TODO_EXT, Consts.MARKDOWN_EXT, Consts.CSV_EXT  -> LuceneHelper.indexFile(context!!, destpath)
            Consts.PNG_EXT, Consts.JPG_EXT, Consts.PDF_EXT, Consts.WEBP_EXT -> LuceneHelper.indexFileTitle(context!!, destpath)
        }
        initFilenames(context!!)
    }

    fun readFile(context: Context?, path: String): String {
        val target = getFile(context, path)
        if (!target.exists()) {
            return ""
        }
        val inputStream = target.inputStream()
        val reader = InputStreamReader(inputStream)
        val result = reader.readText()
        reader.close()
        return result
    }

    fun saveFile(context: Context?, path: String, content: String) {
        val target = getFile(context, path)
        if (!target.exists()) {
            val ops = target.createNewFile()
        }
        var bos: BufferedOutputStream? = null
        try {
            bos = BufferedOutputStream(FileOutputStream(target, false))
            bos.write(content.toByteArray(Charsets.UTF_8))
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            bos?.close()
        }
        when (target.extension) {
            Consts.TODO_EXT, Consts.MARKDOWN_EXT, Consts.CSV_EXT  -> LuceneHelper.indexFile(context!!, path)
            Consts.PNG_EXT, Consts.JPG_EXT, Consts.PDF_EXT, Consts.WEBP_EXT -> LuceneHelper.indexFileTitle(context!!, path)
        }
    }

    fun saveBitmap(context: Context?, path: String, bmp: Bitmap) {
        val target = getFile(context, path)
        if (!target.exists()) {
            val ops = target.createNewFile()
        }
        var bos: FileOutputStream? = null
        try {
            bos = FileOutputStream(target, false)
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, bos)
            bos.flush()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            bos?.close()
        }
        initFilenames(context!!)
    }


    fun getFilename(resolver: ContentResolver, uri: Uri): String {
        val returnCursor = resolver.query(uri, null, null, null, null)!!
        val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }

    fun relativePath(context: Context, path: String): String {
        var target = getFile(context, "")
        return path.replace(target.absolutePath, "")
    }

//    fun getFilesFromAssetDir(context: Context, path: String): List<String> {
//        val lst = context.assets.list(path)
//        return lst.toList()
//    }

//    fun getFileInfo(context: Context, path: String): String {
//        val file = getFile(context, path)
//        return file.absolutePath + "\n" +
//                "Size: " + TextHelper.bytesText(file.length()) + "\n" +
//                "Last modified: " + Timestamp(file.lastModified()).toString()
//    }

//    fun getFileFolder(path: String): String {
//        var parts = path.split(File.separator)
//        parts = parts.dropLast(1)
//        return parts.joinToString(File.separator)
//    }

    fun moveFile(context: Context, filename: String, dest: String) {
        val name = getFile(context, filename).name
        val from = getFile(context, filename)
        val to = getFile(context, dest + File.separator + name)
        from.renameTo(to)
        initFilenames(context)
    }


    fun getAllFiles(context: Context, list: MutableList<File>, root: String = "") {
        if (PrefHelper.getRootDir(context).isNotEmpty()) {
            val files = listFiles(context, root)
            files.forEach {
                if (it.isDirectory) {
                    getAllFiles(context, list, relativePath(context, it.absolutePath))
                } else {
                    list.add(it)
                }
            }
        }
    }

}

