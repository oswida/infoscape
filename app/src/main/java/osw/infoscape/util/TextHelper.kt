package osw.infoscape.util


object TextHelper {

    fun bytesText(size: Long): String {
        if (size > 1000000) {
            return "%.2f Mb".format(size.toFloat() / 1000000.0)
        } else if (size > 1000) {
            return "%.2f Kb".format(size.toFloat() / 1000.0)
        } else {
            return "%d b".format(size)
        }
    }

}