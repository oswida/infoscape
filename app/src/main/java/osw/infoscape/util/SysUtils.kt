package osw.infoscape.util

import android.content.Context
import android.content.res.AssetManager
import android.util.Base64
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import osw.infoscape.db.DbWorker
import osw.infoscape.db.Favourite
import osw.infoscape.db.PdfBookmark
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.nio.ByteBuffer
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec


object SysUtils {

    fun hideKeyboard(context: Context, view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    @ColorInt
    fun getThemeColor(
        context: Context,
        @AttrRes attributeColor: Int
    ): String {
        val value = TypedValue()
        context.theme.resolveAttribute(attributeColor, value, true)
        val hexColor = String.format("#%06X", 0xFFFFFF and value.data)
        return hexColor
    }

    fun dipToPix(context: Context, value: Float): Int {
        val r = context.resources
        val pix = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, r.displayMetrics)
        return pix.toInt()
    }

    private fun adjustKey(key: String): ByteArray {
        val result = ByteArray(16)
        for (i in 0 until key.length) {
            result[i] = key[i].toByte()
        }
        for (i in key.length until 16) {
            result[i] = 0.toByte()
        }
        return result
    }

    fun encrypt(key: String, data: String): String {
        val secureRandom = SecureRandom()
        val secretKey = SecretKeySpec(adjustKey(key), "AES")
        val iv = ByteArray(12)
        secureRandom.nextBytes(iv)
        val cipher = Cipher.getInstance("AES/GCM/NoPadding")
        val parameterSpec = GCMParameterSpec(128, iv) //128 bit auth tag length
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec)
        val cipherText = cipher.doFinal(data.toByteArray(Charsets.UTF_8))
        val byteBuffer = ByteBuffer.allocate(4 + iv.size + cipherText.size)
        byteBuffer.putInt(iv.size)
        byteBuffer.put(iv)
        byteBuffer.put(cipherText)
        val cipherMessage = byteBuffer.array()
        val result = Base64.encodeToString(cipherMessage, Base64.DEFAULT)
        return result
    }

    fun decrypt(key: String, data: String): String {
        try {
            val cipherMessage = Base64.decode(data, Base64.DEFAULT)
            val byteBuffer = ByteBuffer.wrap(cipherMessage)
            val ivLength = byteBuffer.int
            if (ivLength < 12 || ivLength >= 16) {
                throw IllegalArgumentException("invalid iv length")
            }
            val iv = ByteArray(ivLength)
            byteBuffer.get(iv)
            val cipherText = ByteArray(byteBuffer.remaining())
            byteBuffer.get(cipherText)
            val cipher = Cipher.getInstance("AES/GCM/NoPadding")
            cipher.init(
                Cipher.DECRYPT_MODE, SecretKeySpec(
                    adjustKey(key),
                    "AES"
                ), GCMParameterSpec(128, iv)
            )
            val plainText = cipher.doFinal(cipherText)
            return plainText.toString(Charsets.UTF_8)
        } catch (ex: Exception) {
            return ""
        }
    }

    fun exportDb(context: Context) {
        DbWorker.postDbTask(context) { db ->
            val lines = mutableListOf<String>()
            db.pdfBookmarkDao().getAll().forEach {
                lines.add(it.path + "|" + it.name + "|" + it.page.toString())
            }
            FileHelper.saveFile(
                context,
                Consts.EXPORT_DIR + File.separator + "pdfbookmark.txt", lines.joinToString("\n")
            )
        }
        DbWorker.postDbTask(context) { db ->
            val lines = mutableListOf<String>()
            db.favouriteDao().getAll().forEach {
                lines.add(it.path + "|" + it.sticky)
            }
            FileHelper.saveFile(
                context,
                Consts.EXPORT_DIR + File.separator + "favourite.txt", lines.joinToString("\n")
            )
        }
    }

    fun importDb(context: Context) {
        DbWorker.postDbTask(context) { db ->
            var lines = FileHelper.readFile(
                context,
                Consts.EXPORT_DIR + File.separator + "pdfbookmark.txt"
            ).lines()
            db.pdfBookmarkDao().clear()
            lines.forEach {
                val parts = it.split("|")
                if (parts.size == 3) {
                    val bookmark = PdfBookmark(null, parts[0].trim(), parts[1].trim(), parts[2].toInt())
                    db.pdfBookmarkDao().insertAll(bookmark)
                }
            }
        }
        DbWorker.postDbTask(context) { db ->
            var lines = FileHelper.readFile(
                context,
                Consts.EXPORT_DIR + File.separator + "favourite.txt"
            ).lines()
            db.favouriteDao().clear()
            lines.forEach {
                val parts = it.split("|")
                if (parts.size == 2) {
                    val fav = Favourite(null, parts[0].trim(), parts[1].trim().toBoolean())
                    db.favouriteDao().insertAll(fav)
                }
            }
        }
    }

    fun getMime(name: String): String {
        if (name.endsWith(Consts.MARKDOWN_EXT)) {
            return "text/markdown"
        }
        if (name.endsWith(Consts.TEXT_EXT)) {
            return "text/plain"
        }
        if (name.endsWith(Consts.PDF_EXT)) {
            return "application/pdf"
        }
        if (name.endsWith(Consts.PNG_EXT)) {
            return "image/png"
        }
        if (name.endsWith(Consts.JPG_EXT)) {
            return "image/jpg"
        }
        if (name.endsWith(Consts.WEBP_EXT)) {
            return "image/webp"
        }
        return "application/octet-stream"
    }

    @Throws(IOException::class)
    fun readTextFromAsset(assetManager: AssetManager, path: String): String {
        val inputStream = assetManager.open(path)
        val reader = BufferedReader(InputStreamReader(inputStream))
        val stringBuilder = StringBuilder()
        var line = reader.readLine()
        while (line != null) {
            stringBuilder.append(line).append("\n")
            line = reader.readLine()
        }
        reader.close()
        inputStream.close()
        return stringBuilder.toString()
    }


}