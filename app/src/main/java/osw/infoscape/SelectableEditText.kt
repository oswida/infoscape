package osw.infoscape

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.EditText

class SelectableEditText(context: Context, attrs: AttributeSet) : EditText(context, attrs) {

    var storedSelStart: Int = 0
    var storedSelEnd: Int = 0

    fun resetStoredSelection() {
        storedSelStart = 0
        storedSelEnd = 0
    }

    fun getStoredSelectionText(): String {
        if (storedSelStart != 0 && storedSelEnd != 0) {
            return text.toString().substring(storedSelStart, storedSelEnd)
        }
        return ""
    }

    fun replaceStoredSelection(replaceText: String) {
        if (storedSelStart != 0 && storedSelEnd != 0) {
            text.replace(storedSelStart, storedSelEnd, replaceText)
        } else {
            text.insert(selectionStart, replaceText)
        }
    }

    fun replaceSelection(start: Int, end: Int, replaceText: String) {
        if (start != 0 && end != 0) {
            text.replace(start, end, replaceText)
        } else {
            text.insert(start, replaceText)
        }
    }


    override fun onSelectionChanged(selStart: Int, selEnd: Int) {
        super.onSelectionChanged(selStart, selEnd)
        // Only non-empty selections are remembered
        if (selStart != selEnd) {
            storedSelStart = selStart
            storedSelEnd = selEnd
        }
    }

    fun deleteCurrentLine() {
        var start = selectionStart
        var end = selectionEnd
        start--
        while (start >= 0 && text[start] != '\n') {
            start--
        }
        while (end < text.length && text[end] != '\n') {
            end++
        }
        if (start<end-1) {
            replaceSelection(start, end - 1, "")
        }
    }

    fun goToLineStart() {
        var start = selectionStart
        start--
        while (start >= 0 && text[start] != '\n') {
            start--
        }
        setSelection(start+1)
    }

    fun goToLineEnd() {
        var end = selectionEnd
        while (end < text.length && text[end] != '\n') {
            end++
        }
        setSelection(end)
    }

}