package osw.infoscape.theme

import android.content.Context
import br.tiagohm.markdownview.css.styles.Github

class GithubDark(val context: Context, fontsize: Int = 14) : Github() {
    init {
        addRule(
            "body",
            "padding: 10px",
            "color: #fff", "background-color: #333",
            "font-size: $fontsize px"
        )
        addRule("table tr:nth-child(2n)", "background-color: #454341")
        addRule("blockquote", "padding: 0px 20px", "margin: 0 0 20px", "font-size: 14px", "border-left: 5px solid #aaa")
        addRule("a", "color: #ad9361", "text-decoration: none", "word-wrap: break-word");
    }
}