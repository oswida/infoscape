package osw.infoscape

import com.github.barteksc.pdfviewer.link.LinkHandler
import com.github.barteksc.pdfviewer.model.LinkTapEvent
import com.github.chuross.morirouter.MoriRouter
import osw.infoscape.util.Consts

class PdfLinkHandler(val activity: MainActivity) : LinkHandler {

    private val router: MoriRouter? get() = activity.router

    override fun handleLinkEvent(event: LinkTapEvent?) {
        var url = event!!.link.uri
        if (url != null && url.startsWith(Consts.INTERNAL_FILE_SCHEME)) {
            url = url.replace(Consts.INTERNAL_FILE_SCHEME,"")
            if (url.endsWith(".${Consts.MARKDOWN_EXT}")) {
                router?.markdownView(url)!!.launch()
            } else if (url.endsWith(".${Consts.CSV_EXT}")) {
                router?.csvView(url)!!.launch()
            } else if (url.endsWith(".${Consts.JPG_EXT}") || url.endsWith(".${Consts.PNG_EXT}") || url.endsWith(".${Consts.WEBP_EXT}")) {
                router?.imageView(url)!!.launch()
            } else if (url.endsWith(".${Consts.PDF_EXT}")) {
                router?.pdfView(url)!!.launch()
            }
        }
    }

}