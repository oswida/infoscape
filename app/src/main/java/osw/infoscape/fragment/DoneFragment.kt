package osw.infoscape.fragment

import android.os.Bundle
import android.view.*
import android.webkit.WebView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import osw.infoscape.MainActivity
import osw.infoscape.R
import osw.infoscape.util.Consts
import osw.infoscape.util.FileHelper
import osw.infoscape.util.TodoHelper

@RouterPath(name = "doneView")
class DoneFragment : Fragment() {
    @Argument
    var filename: String? = null

    private val router: MoriRouter? get() = (activity as? MainActivity)?.router
    private lateinit var contentView: WebView
    private var currentData = ""
    private var filter = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.done_fragment, container, false)
        contentView = result.findViewById(R.id.contentView)
        refresh()
        return result
    }

    private fun refresh() {
        val text = FileHelper.readFile(context, filename!!)
            .split("\n")
            .filter {
                if (filter.trim().isNotBlank()) {
                    it.contains(filter.trim())
                } else {
                    true
                }
            }.joinToString("\n")
        currentData = TodoHelper.toHtml(context!!, text, true)
        contentView.loadDataWithBaseURL("", currentData, "text/html", "UTF-8", "")
        activity?.title = filename!!.replaceFirst("/", "")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.done_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                refresh()
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.action_view -> {
                val fn = filename!!.replace(".${Consts.TODO_ARCHIVE_EXT}", ".${Consts.TODO_EXT}")
                router?.todoView(fn)!!.launch()
            }
        }
        return true
    }

}