package osw.infoscape.fragment

import android.os.Bundle
import android.os.Environment
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import osw.infoscape.R
import osw.infoscape.util.DialogHelper
import osw.infoscape.util.FileHelper
import osw.infoscape.util.PrefHelper
import java.io.File


class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
        var prefRoot = findPreference(PrefHelper.PREF_ROOT_DIR)
        prefRoot.summary = PrefHelper.getRootDir(context!!)
        prefRoot.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            DialogHelper.selectFolder(context!!, FileHelper.getFile(context, "")) {
                val rt = Environment.getExternalStoragePublicDirectory("")
                val name = it.absolutePath.replace(rt.absolutePath, "")
                PrefHelper.setText(
                    context!!,
                    PrefHelper.PREF_ROOT_DIR, name
                )
                prefRoot.summary = PrefHelper.getRootDir(context!!)
                if (PrefHelper.getTemplateDir(context!!).isBlank()) {
                    PrefHelper.setText(
                        context!!,
                        PrefHelper.PREF_TPL_DIR,
                        "templates"
                    )
                    val prefTplDir = findPreference(PrefHelper.PREF_TPL_DIR)
                    prefTplDir.summary = PrefHelper.getTemplateDir(context!!)
                    FileHelper.initFilenames(context!!)
                }
            }
            true
        }
        val prefWebFont = findPreference(PrefHelper.PREF_WEBVIEW_FONT) as EditTextPreference
        prefWebFont.summary = PrefHelper.getWebViewFont(context!!) + " dp"
        prefWebFont.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { pref, value ->
            prefWebFont.summary = "$value dp"
            true
        }
        val prefTplDir = findPreference(PrefHelper.PREF_TPL_DIR)
        prefTplDir.summary = PrefHelper.getTemplateDir(context!!)
        prefTplDir.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            DialogHelper.selectFolder(context!!, FileHelper.getFile(context, ""), null) {
                val rt = Environment.getExternalStoragePublicDirectory("")
                PrefHelper.setText(
                    context!!,
                    PrefHelper.PREF_TPL_DIR, FileHelper.relativePath(context!!, it.absolutePath)
                )
                prefTplDir.summary = PrefHelper.getTemplateDir(context!!)
            }
            true
        }
        val prefCsvSep = findPreference(PrefHelper.PREF_CSV_SEP) as EditTextPreference
        prefCsvSep.summary = PrefHelper.getCsvSeparator(context!!)
        prefCsvSep.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { pref, value ->
            prefCsvSep.summary = value.toString()
            true
        }
    }

}