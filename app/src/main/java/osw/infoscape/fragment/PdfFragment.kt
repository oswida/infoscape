package osw.infoscape.fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import com.github.barteksc.pdfviewer.PDFView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import osw.infoscape.MainActivity
import osw.infoscape.PdfLinkHandler
import osw.infoscape.R
import osw.infoscape.db.DbWorker
import osw.infoscape.db.LastPos
import osw.infoscape.db.PdfBookmark
import osw.infoscape.util.Consts
import osw.infoscape.util.DialogHelper
import osw.infoscape.util.FileHelper
import osw.infoscape.util.PrefHelper
import java.io.File


@RouterPath(name = "pdfView")
class PdfViewFragment : Fragment() {

    @Argument
    var filename: String? = null

    var pdfView: PDFView? = null
    var currentPage = 0
    var pageCount = 0
    var nm: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        nm = PrefHelper.isNightmode(context!!)
        val view = inflater.inflate(R.layout.pdf_fragment, container, false)
        pdfView = view.findViewById(R.id.pdfView)
        refresh()
        return view
    }

    private fun jump(page: Int) {
        activity?.runOnUiThread {
            pdfView!!.jumpTo(page)
        }
    }

    private fun refresh() {
        pdfView!!
            .fromFile(FileHelper.getFile(context!!, filename!!))
            .enableSwipe(true)
            .enableAntialiasing(true)
            .enableDoubletap(true)
            .swipeHorizontal(true)
            .pageSnap(true)
            .autoSpacing(true)
            .pageFling(true)
            .defaultPage(currentPage)
            .nightMode(nm)
            .enableAnnotationRendering(true)
            .onPageChange { page, pageCount ->
                val pg = page + 1
                currentPage = page
                this.pageCount = pageCount
                activity?.title = "($pg/$pageCount) " + shortName()
            }
            .onLongPress {
                performLongClick(it!!)
            }
            .onLoad {
                if (PrefHelper.reopenLastPos(context!!)) {
                    DbWorker.postDbTask(context) { db ->
                        var lp = db.lastPosDao().findForPath(filename!!)
                        if (lp != null) {
                            jump(lp.pos!!.toInt())
                        }
                    }
                }
            }
            .linkHandler(PdfLinkHandler(activity as MainActivity))
            .load()
    }

    override fun onDestroy() {
        if (PrefHelper.reopenLastPos(context!!)) {
            DbWorker.postDbTask(context) { db ->
                var lp = db.lastPosDao().findForPath(filename!!)
                if (lp == null) {
                    lp = LastPos(null, filename!!, currentPage.toLong())
                    db.lastPosDao().insertAll(lp)
                } else {
                    lp.pos = currentPage.toLong()
                    db.lastPosDao().save(lp)
                }
            }
        }
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.pdf_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.save_bookmark -> {
                DbWorker.postDbTask(context) { db ->
                    val items = mutableListOf<String>()
                    var haveItem = false
                    val bcount = db.pdfBookmarkDao().findForPage(filename!!, currentPage)
                    if (bcount == 0) {
                        items.add("Add bookmark")
                    } else {
                        items.add("Delete bookmark")
                        haveItem = true
                    }
                    val bmlist = db.pdfBookmarkDao().getAll(filename!!)
                    if (bmlist.isNotEmpty()) {
                        items.add("Goto bookmark")
                    }
                    DialogHelper.select(context, "Bookmark action", items.toList()) { option, _ ->
                        when (option) {
                            0 -> {
                                if (haveItem) {
                                    val bmark = db.pdfBookmarkDao().getForPage(filename!!, currentPage)
                                    db.pdfBookmarkDao().delete(bmark)
                                    DialogHelper.info(
                                        context, "PDF Bookmark",
                                        "Bookmark ${bmark.name} deleted"
                                    )
                                } else {
                                    DialogHelper.input(context, "Bookmark name", "", "") {
                                        val bmark = PdfBookmark(null, filename, it, currentPage)
                                        db.pdfBookmarkDao().insertAll(bmark)
                                        DialogHelper.info(
                                            context, "PDF Bookmark",
                                            "Bookmark $it saved"
                                        )
                                    }
                                }
                            }
                            1 -> {
                                if (bmlist.isNotEmpty()) {
                                    DialogHelper.select(context, "Go to", bmlist.map { it.name!! }) { pos, _ ->
                                        activity?.runOnUiThread {
                                            val it = bmlist[pos]
                                            currentPage = it.page!!
                                            activity?.title = "(${currentPage + 1}/$pageCount) " + shortName()
                                            jump(currentPage)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            R.id.goto_page -> {
                DialogHelper.selectInt(
                    context, "Goto page",
                    "Page number", currentPage, 1, pageCount
                ) {
                    val pg = it!!
                    if (pg in 1..(pageCount - 1)) {
                        currentPage = pg - 1
                        activity?.title = "($currentPage+1/$pageCount) " + shortName()
                        jump(currentPage)
                    }
                }
            }
            R.id.pdf_toc -> {
                val toc = pdfView!!.tableOfContents
                if (toc.isNotEmpty()) {
                    DialogHelper.select(context, "Table of Contents", toc.map { it.title }) { pos, name ->
                        currentPage = toc[pos].pageIdx.toInt()
                        activity?.title = "($currentPage+1/$pageCount) " + shortName()
                        jump(currentPage)
                    }
                } else {
                    DialogHelper.info(
                        context,
                        "Table of Contents",
                        "Table of contents is not available for this document"
                    )
                }
            }
            R.id.pdf_nightmode -> {
                nm = !nm
                refresh()
            }
        }
        return true
    }

    private fun shortName(): String {
        if (filename != null) {
            val parts = filename!!.split(File.separator)
            val result = parts.last().replace(".${Consts.PDF_EXT}", "").replace("%20", " ")
            return result
        }
        return ""
    }

    private fun performLongClick(event: MotionEvent) {
        val bar = (activity as MainActivity).supportActionBar!!
        if (bar.isShowing) {
            bar.hide()
        } else {
            bar.show()
        }
    }
}