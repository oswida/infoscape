package osw.infoscape.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.RouterPath
import eu.davidea.flexibleadapter.FlexibleAdapter
import osw.infoscape.MainActivity
import osw.infoscape.R
import osw.infoscape.model.SearchItem
import osw.infoscape.model.SearchModel
import osw.infoscape.util.Consts
import osw.infoscape.util.FileHelper
import osw.infoscape.util.SysUtils

@RouterPath(name = "searchView")
class SearchFragment : Fragment(), FlexibleAdapter.OnItemClickListener {
    private var recyclerView: RecyclerView? = null
    private lateinit var model: SearchModel
    private lateinit var fragmentAdapter: FlexibleAdapter<SearchItem>
    private var filter = ""
    private val router: MoriRouter? get() = (activity as? MainActivity)?.router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.search_fragment, container, false)
        val searchField = result.findViewById<EditText>(R.id.searchText)
        searchField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                filter = s.toString()
                model?.refresh(filter)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
        recyclerView = result.findViewById(R.id.list)
        model = ViewModelProviders.of(this).get(SearchModel::class.java)
        fragmentAdapter = FlexibleAdapter(null)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = fragmentAdapter
        }
        fragmentAdapter.isSwipeEnabled = false
        fragmentAdapter.addListener(this)
        model.itemList.observe(this, Observer<List<SearchItem>> {
            fragmentAdapter.updateDataSet(it)
        })
        activity?.title = "Full text search"
        refresh()
        return result
    }


    fun refresh() {
        model.refresh(filter)
    }

    override fun onItemClick(view: View?, position: Int): Boolean {
        val item = fragmentAdapter.getItem(position)
        if (item != null) {
            SysUtils.hideKeyboard(context!!, view!!)
            val filename = FileHelper.relativePath(context!!, item.file.absolutePath)
            when (item.file.extension) {
                Consts.MARKDOWN_EXT -> {
                    router?.markdownView(filename)!!.launch()
                }
                Consts.PDF_EXT -> {
                    router?.pdfView(filename)!!.launch()
                }
                Consts.JPG_EXT, Consts.PNG_EXT, Consts.WEBP_EXT -> {
                    router?.imageView(filename)!!.launch()
                }
                Consts.TODO_EXT -> {
                    router?.todoView(filename)!!.launch()
                }
            }
        }
        return true
    }

}