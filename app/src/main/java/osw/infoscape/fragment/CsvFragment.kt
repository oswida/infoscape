package osw.infoscape.fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import br.tiagohm.markdownview.MarkdownView
import br.tiagohm.markdownview.css.styles.Github
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import osw.infoscape.MainActivity
import osw.infoscape.R
import osw.infoscape.theme.GithubDark
import osw.infoscape.theme.GithubLight
import osw.infoscape.util.Consts
import osw.infoscape.util.FileHelper
import osw.infoscape.util.PrefHelper
import osw.infoscape.util.SysUtils
import java.lang.StringBuilder

@RouterPath(name = "csvView")
class CsvFragment: Fragment() {
    @Argument
    var filename: String? = null

    private val router: MoriRouter? get() = (activity as? MainActivity)?.router
    private lateinit var contentView: MarkdownView
    private var filter = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.md_fragment, container, false)
        contentView = result.findViewById(R.id.contentView)
        contentView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return true
            }

            override fun onLoadResource(view: WebView?, url: String?) {
                if (url != null && url.startsWith(Consts.INTERNAL_FILE_SCHEME)) {
                    val fl = url.replace(Consts.INTERNAL_FILE_SCHEME, "")
                    router?.markdownView(fl)!!.launch()
                    return
                }
                super.onLoadResource(view, url)
            }
        }
        refresh()
        return result
    }

    private fun refresh() {
        var lines = FileHelper.readFile(context, filename!!).split("\n")
        var text = StringBuilder()
        if (lines.isEmpty()) {
            return
        }
        // header
        val pp = lines[0].split(PrefHelper.getCsvSeparator(context!!))
        text.append(pp.joinToString("|")).append("\n")
        text.append(pp.map { "---" }.joinToString("|")).append("\n")
        lines = lines.drop(1)
        if (filter.trim().isNotBlank()) {
            lines = lines.filter {
                it.contains(filter)
            }
        }
        lines.forEach {
            val parts = it.split(PrefHelper.getCsvSeparator(context!!))
            text.append(parts.joinToString("|")).append("\n")
        }
        var theme: Github = GithubLight(context!!)
        if (PrefHelper.isNightmode(context!!)) {
            theme = GithubDark(context!!)
        }
        theme.addRule(
            "body",
            "font-size: " +
                    SysUtils.dipToPix(context!!, PrefHelper.getWebViewFont(context!!).toFloat()) + "px"
        )
        contentView.addStyleSheet(theme)
        contentView.loadMarkdown(text.toString())
        activity?.title = filename!!.replaceFirst("/", "").replace("%20", " ")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.csv_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                refresh()
                return true
            }
        })
    }
}