package osw.infoscape.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.github.zawadz88.materialpopupmenu.popupMenu
import com.google.android.material.floatingactionbutton.FloatingActionButton
import eu.davidea.flexibleadapter.FlexibleAdapter
import osw.infoscape.MainActivity
import osw.infoscape.R
import osw.infoscape.db.DbWorker
import osw.infoscape.db.Favourite
import osw.infoscape.model.FolderItem
import osw.infoscape.model.FolderModel
import osw.infoscape.util.*
import java.io.File

@RouterPath(name = "documentView")
class DocumentFragment : Fragment(),
    FlexibleAdapter.OnItemClickListener {

    companion object {
        const val IMPORT_FILE_CODE = 200
    }

    @Argument
    var folder: String? = null

    @Argument
    var filext: String? = null

    private var recyclerView: RecyclerView? = null
    private lateinit var model: FolderModel
    private lateinit var fragmentAdapter: FlexibleAdapter<FolderItem>
    private var filter = ""
    private val router: MoriRouter? get() = (activity as? MainActivity)?.router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.folder_fragment, container, false)
        recyclerView = result.findViewById(R.id.list)
        model = ViewModelProviders.of(this).get(FolderModel::class.java)
        fragmentAdapter = FlexibleAdapter(null)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = fragmentAdapter
        }
        fragmentAdapter.isSwipeEnabled = false
        fragmentAdapter.addListener(this)
        model.menuClickFunc = this::itemMenuAction
        model.itemList.observe(this, Observer<List<FolderItem>> {
            fragmentAdapter.updateDataSet(it)
        })
        model.rootPath = folder!!
        refresh()
        val fab = result.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { view ->
            DialogHelper.select(
                context, "Add item",
                listOf("Folder", "Markdown file", "Image file", "Todo file")
            ) { pos, _ ->
                when (pos) {
                    0 -> {
                        DialogHelper.input(
                            context,
                            "Add folder",
                            "Folder name",
                            ""
                        ) {
                            val foldername = model.rootPath + File.separator + it
                            FileHelper.createFolder(context, foldername)
                            model.refresh(filter)
                        }
                    }
                    1 -> {
                        DialogHelper.input(
                            context,
                            "Add markdown file",
                            "File name",
                            ""
                        ) {
                            var filename = model.rootPath + File.separator + it
                            if (!filename.endsWith(".${Consts.MARKDOWN_EXT}")) {
                                filename += ".${Consts.MARKDOWN_EXT}"
                            }
                            FileHelper.createFile(context, filename)
                            FileHelper.saveFile(context, filename, Consts.TEST_CONTENT)
                            model.refresh(filter)
                        }
                    }
                    2 -> {
                        DialogHelper.doubleinput(
                            context, "New image",
                            "Image name", "Dimension <w>x<h> in pixels",
                            "", "600x1200"
                        ) { name, dims ->
                            if (!dims.isBlank() && !name.isBlank()) {
                                val parts = dims.trim().toLowerCase().split("x")
                                if (parts.size == 2) {
                                    val width = parts[0].toIntOrNull()
                                    val height = parts[1].toIntOrNull()
                                    if (width != null && height != null && width > 0 && height > 0) {
                                        val conf = Bitmap.Config.ARGB_8888
                                        val bmp = Bitmap.createBitmap(width, height, conf)
                                        bmp.eraseColor(Color.WHITE)
                                        var filename = model.rootPath + File.separator + name
                                        if (!filename.endsWith(".${Consts.JPG_EXT}")) {
                                            filename = filename.plus(".${Consts.JPG_EXT}")
                                        }
                                        FileHelper.saveBitmap(context, filename, bmp)
                                        model.refresh(filter)
                                    }
                                }
                            }
                        }
                    }
                    3 -> {
                        DialogHelper.input(
                            context,
                            "Add todo file",
                            "File name",
                            ""
                        ) {
                            var filename = model.rootPath + File.separator + it
                            if (!filename.endsWith(".${Consts.TODO_EXT}")) {
                                filename += ".${Consts.TODO_EXT}"
                            }
                            FileHelper.createFile(context, filename)
                            FileHelper.saveFile(context, filename, TodoHelper.EXAMPLE_TODO)
                            model.refresh(filter)
                        }
                    }
//                    4 -> {
//                        DialogHelper.input(
//                            context,
//                            "Add CSV file",
//                            "File name",
//                            ""
//                        ) {
//                            var filename = model.rootPath + File.separator + it
//                            if (!filename.endsWith(".${Consts.CSV_EXT}")) {
//                                filename += ".${Consts.CSV_EXT}"
//                            }
//                            FileHelper.createFile(context, filename)
//                            FileHelper.saveFile(context, filename, Consts.TEST_CSV)
//                            model.refresh(filter)
//                        }
//                    }
                }
            }
        }
        return result
    }

    fun refresh() {
        model.refresh(filter, filext!!)
        val pref = PrefHelper.getRootDir(context!!)
        val title = model.rootPath.replaceFirst(pref, "")
        var ext = ""
        if (filext!!.isNotBlank()) {
            ext = "($filext)"
        }
        activity?.title = title
        if (activity?.title.isNullOrBlank()) {
            activity?.title = "Documents $ext"
        } else {
            activity?.title = "$title $ext"
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.folder_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                model.refresh(filter)
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.action_import -> {
                val itn = Intent(Intent.ACTION_OPEN_DOCUMENT)
                itn.addCategory(Intent.CATEGORY_OPENABLE)
                itn.type = "*/*"
                startActivityForResult(itn, IMPORT_FILE_CODE)
            }
        }
        return true
    }

    fun itemMenuAction(item: FolderItem, view: View) {
        val popup = popupMenu {
            section {
                item {
                    label = "Rename"
                    icon = R.drawable.ic_edit
                    callback = {
                        val filename = model?.rootPath + File.separator + item.file.name
                        DialogHelper.input(
                            context,
                            "Rename item",
                            "New item name",
                            filename
                        ) {
                            DbWorker.postDbTask(context) { db ->
                                db.pdfBookmarkDao().deleteForPath(filename)
                            }
                            FileHelper.renameFile(context, filename, it!!)
                            model.refresh(filter)
                        }
                        model.refresh(filter)
                    }
                }
                item {
                    label = "Move"
                    icon = R.drawable.ic_move
                    callback = {
                        val filename = model?.rootPath + File.separator + item.file.name
                        DialogHelper.selectFolder(context, FileHelper.getFile(context,""),null) {
                            FileHelper.moveFile(context!!, filename, FileHelper.relativePath(context!!, it.absolutePath))
                            model.refresh(filter)
                        }
                    }
                }
                item {
                    label = "Delete"
                    icon = R.drawable.ic_delete
                    callback = {
                        DialogHelper.confirm(
                            context,
                            "Delete item",
                            "Please confirm delete of " + item.file.name,
                            {
                                val filename = model?.rootPath + File.separator + item.file.name
                                DbWorker.postDbTask(context) { db ->
                                    db.pdfBookmarkDao().deleteForPath(filename)
                                }
                                FileHelper.deleteFile(context, filename)
                                model.refresh(filter)
                            },
                            {
                                model.refresh(filter)
                            })
                    }
                }
                item {
                    label = if (item.favourite) "Remove from favourites" else "Add to favourites"
                    icon = R.drawable.ic_star
                    callback = {
                        DbWorker.postDbTask(context) { db ->
                            val fname = FileHelper.relativePath(context!!, item!!.file.absolutePath)
                            val fav = db.favouriteDao().findForPath(fname)
                            if (fav == null) {
                                val fav = Favourite(null, fname)
                                db.favouriteDao().insertAll(fav)
                                DialogHelper.info(context, item.file.name, "Added to favourites")
                            } else {
                                db.favouriteDao().delete(fav)
                                DialogHelper.info(context, item.file.name, "Removed from favourites")
                            }
                            model.refresh(filter)
                        }
                    }
                }
                item {
                    label = "Index"
                    icon = R.drawable.ic_index
                    callback = {
                        val filename = model.rootPath + File.separator + item.file.name
                        when(item.file.extension) {
                            Consts.CSV_EXT, Consts.MARKDOWN_EXT, Consts.TODO_EXT -> {
                                LuceneHelper.indexFile(context!!, filename)
                                DialogHelper.info(context!!, "Index file", "File indexed")
                            }
                            else -> {
                                LuceneHelper.indexFileTitle(context!!, filename)
                                DialogHelper.info(context!!, "Index file", "File indexed")
                            }

                        }
                        model.refresh(filter)
                    }
                }
            }
        }
        popup.show(view.context, view)
    }

    override fun onItemClick(view: View?, position: Int): Boolean {
        val item = fragmentAdapter.getItem(position)
        val filename = model.rootPath + File.separator + item!!.file.name
        if (item!!.file.isDirectory) {
            router?.documentView(filename, filext!!)!!.launch()
        } else {
            when (item.file.extension) {
                Consts.JPG_EXT, Consts.PNG_EXT, Consts.WEBP_EXT -> {
                    router?.imageView(filename)!!.launch()
                }
                Consts.PDF_EXT -> {
                    router?.pdfView(filename)!!.launch()
                }
                Consts.MARKDOWN_EXT -> {
                    router?.markdownView(filename)!!.launch()
                }
                Consts.TODO_EXT -> {
                    router?.todoView(filename)!!.launch()
                }
                Consts.CSV_EXT -> {
                    router?.csvView(filename)!!.launch()
                }
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                IMPORT_FILE_CODE -> {
                    if (resultData!!.data != null) {
                        FileHelper.copyFile(
                            context, resultData.data,
                            model.rootPath + File.separator + FileHelper.getFilename(
                                context!!.contentResolver,
                                resultData.data
                            )
                        )
                        model.refresh(filter)
                    }
                }
            }
        }
    }

}