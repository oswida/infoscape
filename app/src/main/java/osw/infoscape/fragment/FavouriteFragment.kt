package osw.infoscape.fragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.RouterPath
import com.github.zawadz88.materialpopupmenu.popupMenu
import eu.davidea.flexibleadapter.FlexibleAdapter
import osw.infoscape.MainActivity
import osw.infoscape.R
import osw.infoscape.db.DbWorker
import osw.infoscape.model.FavouriteItem
import osw.infoscape.model.FavouriteModel
import osw.infoscape.util.Consts
import osw.infoscape.util.FileHelper

@RouterPath(name = "favouriteView")
class FavouriteFragment : Fragment(),
    FlexibleAdapter.OnItemClickListener {

    private var recyclerView: RecyclerView? = null
    private lateinit var model: FavouriteModel
    private lateinit var fragmentAdapter: FlexibleAdapter<FavouriteItem>
    private var filter = ""
    private val router: MoriRouter? get() = (activity as? MainActivity)?.router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.favourite_fragment, container, false)
        recyclerView = result.findViewById(R.id.list)
        model = ViewModelProviders.of(this).get(FavouriteModel::class.java)
        fragmentAdapter = FlexibleAdapter(null)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = fragmentAdapter
        }
        fragmentAdapter.isSwipeEnabled = false
        fragmentAdapter.addListener(this)
        model.menuClickFunc = this::itemMenuAction
        model.itemList.observe(this, Observer<List<FavouriteItem>> {
            fragmentAdapter.updateDataSet(it)
        })
        refresh()
        return result
    }

    fun refresh() {
        model.refresh(filter)
        activity?.title = "Favourites"
    }

    fun itemMenuAction(item: FavouriteItem, view: View) {
        val popup = popupMenu {
            section {
                item {
                    label = "Remove from favourites"
                    icon = R.drawable.ic_star
                    callback = {
                        DbWorker.postDbTask(context) { db ->
                            db.favouriteDao().delete(item.favourite)
                            model.refresh(filter)
                        }
                    }
                }
            }
        }
        popup.show(view.context, view)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.favourite_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                model.refresh(filter)
                return true
            }
        })
    }

    override fun onItemClick(view: View?, position: Int): Boolean {
        val item = fragmentAdapter.getItem(position)!!
        val file = FileHelper.getFile(context, item.favourite.path!!)
        if (file.isDirectory) {
            router?.documentView(item.favourite.path!!, "")!!.launch()
        } else {
            when (file.extension) {
                Consts.JPG_EXT, Consts.PNG_EXT, Consts.WEBP_EXT -> {
                    router?.imageView(item.favourite.path!!)!!.launch()
                }
                Consts.PDF_EXT -> {
                    router?.pdfView(item.favourite.path!!)!!.launch()
                }
                Consts.MARKDOWN_EXT -> {
                    router?.markdownView(item.favourite.path!!)!!.launch()
                }
                Consts.TODO_EXT -> {
                    router?.todoView(item.favourite.path!!)!!.launch()
                }
            }
        }
        return true
    }


}