package osw.infoscape.fragment

import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import osw.infoscape.MainActivity
import osw.infoscape.R
import osw.infoscape.util.FileHelper
import java.io.IOException


@RouterPath(name = "imageView")
class ImageFragment : Fragment() {

    @Argument
    var filename: String? = null

    private val router: MoriRouter? get() = (activity as? MainActivity)?.router

    private lateinit var imageView: SubsamplingScaleImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val result = inflater.inflate(R.layout.image_fragment, container, false)
        imageView = result.findViewById(R.id.image)
        imageView.setOnLongClickListener {
            val bar = (activity as MainActivity).supportActionBar!!
            if (bar.isShowing) {
                bar.hide()
            } else {
                bar.show()
            }
            true
        }
        reloadImage()
        return result
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.image_menu, menu)
    }

    private fun reloadImage() {
        try {
            val file = FileHelper.getFile(context, filename!!)
            imageView.setImage(ImageSource.uri(Uri.fromFile(file)))
            activity?.title = filename!!.replace("%20", " ")
        } catch (ex: IOException) {

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_rotate -> {
                val rt = imageView.orientation
                when (rt) {
                    SubsamplingScaleImageView.ORIENTATION_0 -> imageView.orientation =
                        SubsamplingScaleImageView.ORIENTATION_90
                    SubsamplingScaleImageView.ORIENTATION_90 -> imageView.orientation =
                        SubsamplingScaleImageView.ORIENTATION_180
                    SubsamplingScaleImageView.ORIENTATION_180 -> imageView.orientation =
                        SubsamplingScaleImageView.ORIENTATION_270
                    else -> imageView.orientation = SubsamplingScaleImageView.ORIENTATION_0
                }
            }
            R.id.action_edit -> {
                router?.imageEdit(filename!!)!!.launch()
            }
        }
        return true
    }
}