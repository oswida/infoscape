package osw.infoscape.fragment

import android.os.Bundle
import android.view.*
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import br.tiagohm.markdownview.MarkdownView
import br.tiagohm.markdownview.css.styles.Github
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.options.MutableDataSet
import osw.infoscape.MainActivity
import osw.infoscape.R
import osw.infoscape.db.DbWorker
import osw.infoscape.db.LastPos
import osw.infoscape.theme.GithubDark
import osw.infoscape.theme.GithubLight
import osw.infoscape.util.*


@RouterPath(name = "markdownView")
class MdFragment : Fragment() {
    @Argument
    var filename: String? = null

    private val router: MoriRouter? get() = (activity as? MainActivity)?.router
    private lateinit var contentView: MarkdownView
    private var currentData = ""
    private var filter = ""
    private val hiddenMap = mutableMapOf<Int, String>()
    private val linkMap = mutableMapOf<String, String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.md_fragment, container, false)
        contentView = result.findViewById(R.id.contentView)
        contentView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return true
            }

            override fun onLoadResource(view: WebView?, url: String?) {
                if (url != null && url.startsWith(Consts.INTERNAL_FILE_SCHEME)) {
                    val fl = url.replace(Consts.INTERNAL_FILE_SCHEME, "")
                    if (fl.endsWith(".${Consts.MARKDOWN_EXT}")) {
                        router?.markdownView(fl)!!.launch()
                    } else if (fl.endsWith(".${Consts.CSV_EXT}")) {
                        router?.csvView(fl)!!.launch()
                    } else if (fl.endsWith(".${Consts.JPG_EXT}") || fl.endsWith(".${Consts.PNG_EXT}") || fl.endsWith(".${Consts.WEBP_EXT}")) {
                        router?.imageView(fl)!!.launch()
                    } else if (fl.endsWith(".${Consts.PDF_EXT}")) {
                        router?.pdfView(fl)!!.launch()
                    }
                    return
                } else if (url != null && url.startsWith(Consts.INTERNAL_EDIT_TEXT_SCHEME)) {
                    view!!.stopLoading()
                    val parts = url.replace(Consts.INTERNAL_EDIT_TEXT_SCHEME, "").split("/")
                    DialogHelper.input(view.context, "Edit", "", parts[1]) {
                        activity?.runOnUiThread {
                            updateEditable(parts[0].toInt(), it!!, Consts.EDITABLE_TEXT_MARK)
                        }
                    }
                } else if (url != null && url.startsWith(Consts.INTERNAL_EDIT_INT_SCHEME)) {
                    view!!.stopLoading()
                    val parts = url.replace(Consts.INTERNAL_EDIT_INT_SCHEME, "").split("/")
                    DialogHelper.inputInt(view.context, "Edit", "", parts[1].toInt()) {
                        activity?.runOnUiThread {
                            updateEditable(parts[0].toInt(), it!!.toString(), Consts.EDITABLE_INT_MARK)
                        }
                    }
                } else if (url != null && url.startsWith(Consts.INTERNAL_EDIT_FLOAT_SCHEME)) {
                    view!!.stopLoading()
                    val parts = url.replace(Consts.INTERNAL_EDIT_FLOAT_SCHEME, "").split("/")
                    DialogHelper.inputFloat(view.context, "Edit", "", parts[1].toFloat()) {
                        activity?.runOnUiThread {
                            updateEditable(parts[0].toInt(), it!!.toString(), Consts.EDITABLE_FLOAT_MARK)
                        }
                    }
                } else if (url != null && url.startsWith(Consts.INTERNAL_EDIT_ONOFF_SCHEME)) {
                    view!!.stopLoading()
                    val parts = url.replace(Consts.INTERNAL_EDIT_ONOFF_SCHEME, "").split("/")
                    updateEditable(parts[0].toInt(), "?${parts[2]}|${parts[1]}?", Consts.EDITABLE_ONOFF_MARK, true)
                } else if (url != null && url.startsWith(Consts.INTERNAL_HIDDEN_SCHEME)) {
                    view!!.stopLoading()
                    val parts = url.replace(Consts.INTERNAL_HIDDEN_SCHEME, "").split("/")
                    val text = hiddenMap[parts[0].toInt()]
                    if (text != null) {
                        DialogHelper.info(context, parts[1], text)
                    }
                }
                super.onLoadResource(view, url)
            }
        }
        contentView.setOnLongClickListener {
            val bar = (activity as MainActivity).supportActionBar!!
            if (bar.isShowing) {
                bar.hide()
            } else {
                bar.show()
            }
            true
        }
        refresh()
        return result
    }


    override fun onDestroy() {
        val bar = (activity as MainActivity).supportActionBar!!
        bar.show()
        if (PrefHelper.reopenLastPos(context!!)) {
            DbWorker.postDbTask(context) { db ->
                var lp = db.lastPosDao().findForPath(filename!!)
                if (lp == null) {
                    lp = LastPos(null, filename!!, contentView.scrollY.toLong())
                    db.lastPosDao().insertAll(lp)
                } else {
                    lp.pos = contentView.scrollY.toLong()
                    db.lastPosDao().save(lp)
                }
            }
        }
        super.onDestroy()
    }

    private fun refresh() {
        var text = FileHelper.readFile(context, filename!!)
        var theme: Github = GithubLight(context!!)
        if (PrefHelper.isNightmode(context!!)) {
            theme = GithubDark(context!!)
        }
        theme.addRule(
            "body",
            "font-size: " +
                    SysUtils.dipToPix(context!!, PrefHelper.getWebViewFont(context!!).toFloat()) + "px"
        )
        contentView.addStyleSheet(theme)

        prepareLinkList(text)

        if (PrefHelper.isAutolinks(context!!)) {
            text = prepareCard(text)
        }
        currentData = prepareEditable(text)
        currentData = prepareHidden(currentData)

        contentView.loadMarkdown(currentData)
        activity?.title = filename!!.replaceFirst("/", "").replace("%20", " ")
//        if (PrefHelper.reopenLastPos(context!!)) {
//            DbWorker.postDbTask(context) { db ->
//                val lp = db.lastPosDao().findForPath(filename!!)
//                if (lp != null) {
//                    contentView.scrollTo(0, lp.pos!!.toInt())
//                }
//            }
//        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.md_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                if (filter.isNotBlank()) {
                    contentView.findAllAsync(filter)
                } else {
                    contentView.clearMatches()
                }
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_edit -> {
                router?.markdownEdit(filename!!)!!.launch()
            }
            R.id.action_toc -> {
                var text = FileHelper.readFile(context, filename!!)
                val options = MutableDataSet()
                val parser = Parser.builder(options).build()
                val doc = parser.parse(text)
                val list = mutableListOf<String>()
                doc.childIterator.forEach {
                    if (it.nodeName == "Heading") {
                        list.add(it.chars.toString().replace("#", "."))
                    }
                }
                DialogHelper.select(context!!, "Table of Contents", list) { pos, name ->
                    contentView.findAllAsync(name.replace(".", "").trim())
                    contentView.clearMatches()
                }
            }
            R.id.action_links -> {
                DialogHelper.select(context, "Goto link", linkMap.keys.toList()) { pos, item ->
                    val url = linkMap[item]!!
                    val idx = url.lastIndexOf(".")
                    val ext = url.substring(idx + 1)
                    when (ext) {
                        Consts.JPG_EXT, Consts.PNG_EXT, Consts.WEBP_EXT -> router?.imageView(url)!!.launch()
                        Consts.PDF_EXT -> router?.pdfView(url)!!.launch()
                        Consts.MARKDOWN_EXT -> router?.markdownView(url)!!.launch()
                        Consts.CSV_EXT -> router?.csvView(url)!!.launch()
                        Consts.TODO_EXT -> router?.todoView(url)!!.launch()
                    }
                }
            }
        }
        return true
    }

    fun prepareCard(text: String): String {
        var result = text
        FileHelper.filenames.forEach { fname ->
            val word = Regex("(?:^|\\W)(${fname.nameWithoutExtension})(?:\\W|$)")
            val path = FileHelper.relativePath(context!!, fname.absolutePath).replace(" ", "%20")
            var match = word.find(result)
            while (match != null) {
                val rpm = "[${match.groups[1]!!.value}](${Consts.INTERNAL_FILE_SCHEME}$path)"
                result = result
                    .replaceRange(match.groups[1]!!.range, rpm)
                match = word.find(result, match.range.start + rpm.length)
            }
        }
        return result
    }


    fun prepareEditable(text: String): String {
        var result = text
        var match = Regex(Consts.EDITABLE_FLOAT_MARK).find(result)
        var cnt = 0
        while (match != null) {
            val ref =
                "[${match.groups[1]!!.value}](${Consts.INTERNAL_EDIT_FLOAT_SCHEME}$cnt/${match.groups[1]!!.value})"
            result = result.replaceRange(match.range, ref)
            cnt++
            match = Regex(Consts.EDITABLE_FLOAT_MARK).find(result)
        }
        match = Regex(Consts.EDITABLE_INT_MARK).find(result)
        cnt = 0
        while (match != null) {
            val ref = "[${match.groups[1]!!.value}](${Consts.INTERNAL_EDIT_INT_SCHEME}$cnt/${match.groups[1]!!.value})"
            result = result.replaceRange(match.range, ref)
            cnt++
            match = Regex(Consts.EDITABLE_INT_MARK).find(result)
        }
        match = Regex(Consts.EDITABLE_TEXT_MARK).find(result)
        cnt = 0
        while (match != null) {
            val ref = "[${match.groups[1]!!.value}](${Consts.INTERNAL_EDIT_TEXT_SCHEME}$cnt/${match.groups[1]!!.value})"
            result = result.replaceRange(match.range, ref)
            cnt++
            match = Regex(Consts.EDITABLE_TEXT_MARK).find(result)
        }
        match = Regex(Consts.EDITABLE_ONOFF_MARK).find(result)
        cnt = 0
        while (match != null) {
            val ref =
                "[${match.groups[1]!!.value}](${Consts.INTERNAL_EDIT_ONOFF_SCHEME}$cnt/${match.groups[1]!!.value}/${match.groups[2]!!.value})"
            result = result.replaceRange(match.range, ref)
            cnt++
            match = Regex(Consts.EDITABLE_ONOFF_MARK).find(result)
        }
        return result
    }

    fun prepareHidden(text: String): String {
        hiddenMap.clear()
        var result = text
        var match = Regex(Consts.HIDDEN_MARK).find(result)
        var cnt = 0
        while (match != null) {
            val ref = "[${match.groups[1]!!.value}](${Consts.INTERNAL_HIDDEN_SCHEME}$cnt/${match.groups[1]!!.value})"
            result = result.replaceRange(match.range, ref)
            hiddenMap[cnt] = match.groups[2]!!.value
            cnt++
            match = Regex(Consts.HIDDEN_MARK).find(result)
        }
        return result
    }

    fun updateEditable(pos: Int, value: String, mark: String, whole: Boolean = false) {
        var result = FileHelper.readFile(context, filename!!)
        var matches = Regex(mark).findAll(result)
        if (pos < matches.count()) {
            val match = matches.elementAt(pos)
            if (whole) {
                result = result.replaceRange(match.range, value)
            } else {
                result = result.replaceRange(match.groups[1]!!.range, value)
            }
            FileHelper.saveFile(context, filename!!, result)
            refresh()
        }
    }

    private fun prepareLinkList(text: String) {
        linkMap.clear()
        val reg = Regex("\\[([^]]+)]\\(([^)]+)\\)")
        val matches = reg.findAll(text)
        matches.forEach {
            val url = it.groups[2]!!.value
            if (url.startsWith(Consts.INTERNAL_FILE_SCHEME)) {
                linkMap[it.groups[1]!!.value] = url.replace(Consts.INTERNAL_FILE_SCHEME, "")
            }
        }
    }


}