package osw.infoscape.fragment

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.raed.drawingview.DrawingView
import com.raed.drawingview.brushes.Brushes
import osw.infoscape.MainActivity
import osw.infoscape.R
import osw.infoscape.util.DialogHelper
import osw.infoscape.util.FileHelper
import osw.infoscape.util.SysUtils
import java.io.IOException


@RouterPath(name = "imageEdit")
class ImageEditFragment : Fragment() {

    @Argument
    var filename: String? = null

    private val router: MoriRouter? get() = (activity as? MainActivity)?.router

    private lateinit var imageView: DrawingView
    private lateinit var colorItem: MenuItem
    private lateinit var brushItem: MenuItem
    private lateinit var sizeItem: MenuItem
    private lateinit var zoomItem: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val result = inflater.inflate(R.layout.image_edit_fragment, container, false)
        imageView = result.findViewById(R.id.image)
        imageView.brushSettings.color = Color.BLACK
        imageView.brushSettings.selectedBrush = Brushes.PEN
        imageView.brushSettings.selectedBrushSize = 0.2f
        reloadImage()
        return result
    }

    private fun reloadImage() {
        try {
            val file = FileHelper.getFile(context, filename!!)
            val ims = file.inputStream()
            val d = Drawable.createFromStream(ims, null) as BitmapDrawable
            imageView.setBackgroundImage(d.bitmap)
            ims.close()
            activity?.title = filename!!.replace("%20", " ")
        } catch (ex: IOException) {
            Log.e("ImageEditFragment", ex.message)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.image_edit_menu, menu)
        colorItem = menu.findItem(R.id.setColor)
        colorItem.icon.setColorFilter(imageView!!.brushSettings.color, PorterDuff.Mode.SRC_ATOP)
        brushItem = menu.findItem(R.id.setBrush)
        updateBrushIcon()
        sizeItem = menu.findItem(R.id.setSize)
        updateSizeIcon()
        zoomItem = menu.findItem(R.id.setZoom)
    }

    private fun updateSizeIcon() {
        when (imageView.brushSettings.selectedBrushSize) {
            0.2f -> sizeItem.setIcon(R.drawable.ic_size_1)
            0.4f -> sizeItem.setIcon(R.drawable.ic_size_2)
            0.6f -> sizeItem.setIcon(R.drawable.ic_size_3)
            0.8f -> sizeItem.setIcon(R.drawable.ic_size_4)
            1.0f -> sizeItem.setIcon(R.drawable.ic_size_5)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.setColor -> {
                DialogHelper.selectColor(context, "Brush color", imageView.brushSettings.color) {
                    imageView.brushSettings.color = it
                    colorItem.icon.setColorFilter(imageView!!.brushSettings.color, PorterDuff.Mode.SRC_ATOP)
                }
            }
            R.id.setBrush -> {
                DialogHelper.select(
                    context,
                    "Brush type",
                    listOf("Air brush", "Calligraphy", "Eraser", "Pen", "Pencil")
                ) { pos, name ->
                    when (pos) {
                        0 -> imageView.brushSettings.selectedBrush = Brushes.AIR_BRUSH
                        1 -> imageView.brushSettings.selectedBrush = Brushes.CALLIGRAPHY
                        2 -> imageView.brushSettings.selectedBrush = Brushes.ERASER
                        3 -> imageView.brushSettings.selectedBrush = Brushes.PEN
                        4 -> imageView.brushSettings.selectedBrush = Brushes.PENCIL
                    }
                    updateBrushIcon()
                }
            }
            R.id.save -> {
                val bitmap = imageView.exportDrawing()
                FileHelper.saveBitmap(context, filename!!, bitmap)
                router?.pop()
            }
            R.id.clean -> {
                DialogHelper.confirm(context, "Clear image", "Please confirm image clear", {
                    val bmp = imageView.exportDrawing()
                    bmp.eraseColor(Color.WHITE)
                    imageView.setBackgroundImage(bmp)
                })
            }
            R.id.setSize -> {
                DialogHelper.select(context, "Brush size", listOf("1", "2", "3", "4", "5")) { pos, name ->
                    when (pos) {
                        0 -> imageView.brushSettings.selectedBrushSize = 0.2f
                        1 -> imageView.brushSettings.selectedBrushSize = 0.4f
                        2 -> imageView.brushSettings.selectedBrushSize = 0.6f
                        3 -> imageView.brushSettings.selectedBrushSize = 0.8f
                        4 -> imageView.brushSettings.selectedBrushSize = 1.0f
                    }
                    updateSizeIcon()
                }
            }
            R.id.setZoom -> {
                if (imageView.isInZoomMode) {
                    imageView.exitZoomMode()
                    imageView.resetZoom()
                    zoomItem.icon.setColorFilter(
                        Color.parseColor(SysUtils.getThemeColor(context!!, android.R.attr.colorBackground)),
                        PorterDuff.Mode.SRC_ATOP
                    )
                } else {
                    imageView.enterZoomMode()
                    zoomItem.icon.setColorFilter(
                        Color.RED,
                        PorterDuff.Mode.SRC_ATOP
                    )
                }
            }
        }
        return true
    }

    private fun updateBrushIcon() {
        when (imageView.brushSettings.selectedBrush) {
            Brushes.AIR_BRUSH -> {
                brushItem.setIcon(R.drawable.ic_brush_air)
            }
            Brushes.PENCIL -> {
                brushItem.setIcon(R.drawable.ic_brush_pencil)
            }
            Brushes.PEN -> {
                brushItem.setIcon(R.drawable.ic_brush_pen)
            }
            Brushes.ERASER -> {
                brushItem.setIcon(R.drawable.ic_brush_eraser)
            }
            Brushes.CALLIGRAPHY -> {
                brushItem.setIcon(R.drawable.ic_brush_calligraphy)
            }
        }
    }

}
