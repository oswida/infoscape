package osw.infoscape.fragment

import android.os.Bundle
import android.view.*
import android.webkit.JavascriptInterface
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.google.android.material.floatingactionbutton.FloatingActionButton
import eu.davidea.flexibleadapter.FlexibleAdapter
import osw.infoscape.MainActivity
import osw.infoscape.R
import osw.infoscape.model.TodoLineItem
import osw.infoscape.model.TodoLineModel
import osw.infoscape.util.Consts
import osw.infoscape.util.DialogHelper
import osw.infoscape.util.FileHelper
import osw.infoscape.util.TodoHelper
import java.util.Date

@RouterPath(name = "todoView")
class TodoFragment : Fragment(),
    FlexibleAdapter.OnItemSwipeListener,
    FlexibleAdapter.OnItemLongClickListener {

    @Argument
    var filename: String? = null

    private var recyclerView: RecyclerView? = null
    private lateinit var model: TodoLineModel
    private lateinit var fragmentAdapter: FlexibleAdapter<TodoLineItem>
    private var filter = ""
    private val router: MoriRouter? get() = (activity as? MainActivity)?.router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.todo_fragment, container, false)
        val fab = result.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            DialogHelper.input(context!!, "New todo item", "Item text", "") {
                if (it != null && it.isNotBlank()) {
                    TodoHelper.insert(context!!, filename!!, it)
                    refresh()
                }
            }
        }
        recyclerView = result.findViewById(R.id.list)
        model = ViewModelProviders.of(this).get(TodoLineModel::class.java)
        fragmentAdapter = FlexibleAdapter(null)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = fragmentAdapter
        }
        fragmentAdapter.isSwipeEnabled = true
        fragmentAdapter.addListener(this)
        model.itemList.observe(this, Observer<List<TodoLineItem>> {
            fragmentAdapter.updateDataSet(it)
        })
        model.filename = filename
        refresh()
        return result
    }

    private fun refresh() {
        activity?.runOnUiThread {
            model.refresh(filter)
            activity?.title = filename!!.replaceFirst("/", "")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.todo_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                refresh()
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.action_archive_view -> {
                val fn = filename!!.replace(".${Consts.TODO_EXT}", ".${Consts.TODO_ARCHIVE_EXT}")
                router?.doneView(fn)!!.launch()
            }
            R.id.action_archive_all -> {
                DialogHelper.confirm(
                    context,
                    "Archive all completed tasks",
                    "Please confirm archiving of all completed tasks", {
                        TodoHelper.archiveAll(context!!, filename!!)
                        refresh()
                    },
                    {})
            }
            R.id.action_sort -> {
                DialogHelper.select(context!!, "", listOf("Creation date", "Priority", "Text")) { pos, _ ->
                    when (pos) {
                        0 -> TodoHelper.sort(context!!, filename!!, TodoHelper.SORT_CREATION_DATE, true)
                        1 -> TodoHelper.sort(context!!, filename!!, TodoHelper.SORT_PRIO)
                        else -> TodoHelper.sort(context!!, filename!!, TodoHelper.SORT_TEXT)
                    }
                    refresh()
                }
            }
        }
        return true
    }

    @JavascriptInterface
    fun showMenu(data: String) {
        val line = data.toInt()
        val lines = FileHelper.readFile(context, filename!!).split("\n")
        if (line < lines.size) {
            val cl = lines[line].trim()
            DialogHelper.select(
                view!!.context, cl.take(35),
                listOf("Set priority", "Toggle done", "Edit", "Delete", "Archive")
            ) { pos, item ->
                when (pos) {
                    0 -> {
                        DialogHelper.select(
                            context!!, "",
                            listOf(
                                "None",
                                "Priority A",
                                "Priority B",
                                "Priority C",
                                "Priority D",
                                "Priority E",
                                "Priority F"
                            )
                        ) { p, i ->
                            when (p) {
                                0 -> {
                                    TodoHelper.setPrio(
                                        context!!,
                                        filename!!,
                                        cl,
                                        ""
                                    )
                                    refresh()
                                }
                                else -> {
                                    TodoHelper.setPrio(
                                        context!!,
                                        filename!!,
                                        cl,
                                        i.replace("Priority ", "")
                                    )
                                    refresh()
                                }
                            }
                        }
                    }
                    1 -> {
                        TodoHelper.toggleDone(context!!, filename!!, cl)
                        refresh()
                    }
                    2 -> {
                        DialogHelper.input(
                            context!!,
                            "Edit todo item",
                            "Item text",
                            lines[line.toInt()]
                        ) {
                            TodoHelper.update(
                                context!!, filename!!,
                                cl, it!!
                            )
                            refresh()
                        }
                    }
                    3 -> {
                        DialogHelper.confirm(
                            context!!,
                            "Delete todo item",
                            "Please confirm deletion of: $cl",
                            {
                                TodoHelper.delete(context!!, filename!!, cl)
                                refresh()
                            }, {})
                    }
                    4 -> {
                        if (cl.startsWith("x")) {
                            DialogHelper.confirm(
                                context!!,
                                "Archive todo item",
                                "Please confirm archiving of: $cl",
                                {
                                    TodoHelper.archive(context!!, filename!!, cl)
                                    refresh()
                                }, {})
                        }
                    }
                }
            }
        }
    }

    override fun onActionStateChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
    }

    private fun updateItem(item: TodoLineItem) {
        TodoHelper.update(context!!, filename!!, item.line, item.toString())
    }

    override fun onItemSwipe(position: Int, direction: Int) {
        val item = fragmentAdapter.getItem(position)
        when (direction) {
            Consts.SWIPE_RIGHT -> {
                item!!.completed = !item.completed
                if (item.completed) {
                    item.priority = ""
                    item.finished = Date()
                } else {
                    item.finished = null
                }
                updateItem(item)
                model.refresh(filter)
            }
            Consts.SWIPE_LEFT -> {
                DialogHelper.input(context, "Edit todo item", "", item!!.text) {
                    item.text = it!!
                    updateItem(item)
                    model.refresh(filter)
                }
                model.refresh(filter)
            }
        }
    }

    override fun onItemLongClick(position: Int) {
        val item = fragmentAdapter.getItem(position)
        var actions = listOf(
            "Set priority", "Delete"
        )
        if (item!!.completed) {
            actions = actions.drop(1)
        }
        DialogHelper.select(
            context,
            "Action",
            actions
        ) { pos, name ->
            when (name) {
                "Set priority" -> {
                    DialogHelper.select(context, "Priority", listOf("A", "B", "C", "D", "E")) { pos, name ->
                        item.priority = name
                        updateItem(item)
                        model.refresh(filter)
                    }
                }
                "Delete" -> {
                    DialogHelper.confirm(context, "Delete",
                        "Please confirm deletion of " + item.text, {
                            TodoHelper.delete(context!!, filename!!, item.line)
                            model.refresh(filter)
                        }, {})
                }
            }
        }
    }


}