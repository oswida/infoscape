package osw.infoscape.fragment

import android.os.Bundle
import android.view.*
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.github.zawadz88.materialpopupmenu.popupMenu
import osw.infoscape.MainActivity
import osw.infoscape.R
import osw.infoscape.SelectableEditText
import osw.infoscape.util.*

@RouterPath(name = "markdownEdit")
class MdEditFragment : Fragment() {

    @Argument
    var filename: String? = null

    private val router: MoriRouter? get() = (activity as? MainActivity)?.router
    private lateinit var editView: SelectableEditText
    var blockIME = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.md_edit_fragment, container, false)
        editView = result.findViewById(R.id.editView)
        var btn = result.findViewById<ImageButton>(R.id.btn_textaction)
        btn.setOnClickListener {
            val popup = popupMenu {
                section {
                    item {
                        label = "Bold"
                        callback = {
                            replaceSelection("**", "**")
                        }
                    }
                    item {
                        label = "Italic"
                        callback = {
                            replaceSelection("*", "*")
                        }
                    }
                    item {
                        label = "Strikethrough"
                        callback = {
                            replaceSelection("~~", "~~")
                        }
                    }
                    item {
                        label = "Code"
                        callback = {
                            replaceSelection("`", "`")
                        }
                    }
                    item {
                        label = "Mark"
                        callback = {
                            replaceSelection("==", "==")
                        }
                    }
                    item {
                        label = "Quote"
                        callback = {
                            replaceSelection("> ", "")
                        }
                    }
                    item {
                        label = "Hidden"
                        callback = {
                            replaceSelection("\n@ title | ", "\n@")
                        }
                    }
                }
            }
            popup.show(context!!, it)
        }
        btn = result.findViewById(R.id.btn_hline)
        btn.setOnClickListener {
            replaceSelection("\n---", "\n")
        }
        btn = result.findViewById(R.id.btn_label)
        btn.setOnClickListener {
            val popup = popupMenu {
                section {
                    item {
                        label = "Label"
                        callback = {
                            replaceSelection("--", "--")
                        }
                    }
                    item {
                        label = "Success"
                        callback = {
                            replaceSelection("---", "---")
                        }
                    }
                    item {
                        label = "Warning"
                        callback = {
                            replaceSelection("----", "----")
                        }
                    }
                    item {
                        label = "Danger"
                        callback = {
                            replaceSelection("-----", "-----")
                        }
                    }
                    item {
                        label = "Editable text"
                        callback = {
                            replaceSelection("!", "!")
                        }
                    }
                    item {
                        label = "Editable integer"
                        callback = {
                            replaceSelection("!=", "!")
                        }
                    }
                    item {
                        label = "Editable float"
                        callback = {
                            replaceSelection("!==", "!")
                        }
                    }
                }
            }
            popup.show(context!!, it)
        }
        btn = result.findViewById(R.id.btn_header)
        btn.setOnClickListener {
            val popup = popupMenu {
                section {
                    item {
                        label = "H1"
                        callback = {
                            replaceSelection("\n# ", "\n")
                        }
                    }
                    item {
                        label = "H2"
                        callback = {
                            replaceSelection("\n## ", "\n")
                        }
                    }
                    item {
                        label = "H3"
                        callback = {
                            replaceSelection("\n### ", "\n")
                        }
                    }
                    item {
                        label = "H4"
                        callback = {
                            replaceSelection("\n#### ", "\n")
                        }
                    }
                }
            }
            popup.show(context!!, it)
        }
        btn = result.findViewById(R.id.btn_list)
        btn.setOnClickListener {
            val popup = popupMenu {
                section {
                    item {
                        label = "Unordered list"
                        callback = {
                            replaceSelection("\n- \n- \n- \n", "\n")
                        }
                    }
                    item {
                        label = "Ordered list"
                        callback = {
                            replaceSelection("\n1. \n2. \n3. \n", "\n")
                        }
                    }
                    item {
                        label = "Checklist"
                        callback = {
                            replaceSelection("\n- [ ] t\n- [ ] t\n- [ ] t\n", "\n")
                        }
                    }
                }
            }
            popup.show(context!!, it)
        }
        btn = result.findViewById(R.id.btn_table)
        btn.setOnClickListener {
            val popup = popupMenu {
                section {
                    item {
                        label = "Centered table"
                        callback = {
                            replaceSelection("\nt1|t2|t3\n:---:|:---:|:---:\nc1|c2|c3", "\n")
                        }
                    }
                    item {
                        label = "Left-aligned table"
                        callback = {
                            replaceSelection("\nt1|t2|t3\n---|---|---\nc1|c2|c3", "\n")
                        }
                    }
                }
            }
            popup.show(context!!, it)
        }
        btn = result.findViewById(R.id.btn_link)
        btn.setOnClickListener {
            val popup = popupMenu {
                section {
                    item {
                        label = "Generic link"
                        callback = {
                            replaceSelection("[description]()", "")
                        }
                    }
                    item {
                        label = "External link"
                        callback = {
                            DialogHelper.doubleinput(
                                context,
                                "External link",
                                "URL",
                                "Description",
                                "",
                                ""
                            ) { one, two ->
                                var url = one
                                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                                    url = "http://$url"
                                }
                                replaceSelection("[$two]($url)", "")
                            }
                        }
                    }
                    item {
                        label = "File link"
                        callback = {
                            DialogHelper.selectFile(
                                context,
                                FileHelper.getFile(context, ""),
                                null
                            ) {
                                replaceSelection(
                                    "[" + it.name + "](" + Consts.INTERNAL_FILE_SCHEME +
                                            FileHelper.relativePath(context!!, it.absolutePath) +
                                            ")", ""
                                )
                            }
                        }
                    }
                    item {
                        label = "Image"
                        callback = {
                            DialogHelper.selectFile(
                                context,
                                FileHelper.getFile(context, ""),
                                null
                            ) {
                                replaceSelection(
                                    "![" + it.name + "](" + Consts.EXTERNAL_FILE_SCHEME +
                                            it.absolutePath +
                                            ")", ""
                                )
                            }
                        }
                    }
                }
            }
            popup.show(context!!, it!!)
        }
        btn = result.findViewById(R.id.btn_keyboard)
        btn.setOnClickListener {
            val popup = popupMenu {
                section {
                    item {
                        label = "Clear all"
                        callback = {
                            DialogHelper.confirm(context, "Clear all text",
                                "Please confirm whole text deletion", {
                                    editView.text.clear()
                                }, {})
                        }
                    }
                    item {
                        label = "Delete line"
                        callback = {
                            editView.deleteCurrentLine()
                        }
                    }
                    item {
                        label = "Goto line start"
                        callback = {
                            editView.goToLineStart()
                        }
                    }
                    item {
                        label = "Goto line end"
                        callback = {
                            editView.goToLineEnd()
                        }
                    }
                }
            }
            popup.show(context!!, it!!)
        }
        btn = result.findViewById(R.id.btn_insert)
        btn.setOnClickListener {
            val popup = popupMenu {
                section {
                    item {
                        label = "Template from file"
                        callback = {
                            DialogHelper.selectFile(
                                context,
                                FileHelper.getFile(context, PrefHelper.getTemplateDir(context!!)),
                                null
                            ) {
                                val text = FileHelper.readFile(
                                    context,
                                    FileHelper.relativePath(context!!, it.absolutePath)
                                )
                                replaceSelection(text, "")
                            }
                        }
                    }
                    item {
                        label = "Emoji"
                        callback = {
                            val items = SysUtils.readTextFromAsset(context!!.assets, "emoji.csv").split("\n")
                            DialogHelper.autocomplete(context, "Emoji", items) {
                                replaceSelection(":$it:", "")
                            }
                        }
                    }
                }
            }
            popup.show(context!!, it!!)
        }
        refresh()
        return result
    }

    fun refresh() {
        val text = FileHelper.readFile(context, filename!!)
        editView.setText(text)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.md_edit_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                SysUtils.hideKeyboard(context!!, view!!)
                FileHelper.saveFile(context, filename!!, editView.text.toString())
                router?.markdownView(filename!!)!!.launch()
            }
        }
        return true
    }

    private fun replaceSelection(prefix: String, suffix: String) {
        val txt = editView.getStoredSelectionText() //.text.substring(editView.selectionStart, editView.selectionEnd)
        val selStart = editView.selectionStart
        editView.replaceStoredSelection("$prefix$txt$suffix")
        editView.setSelection(selStart + prefix.length)
        editView.resetStoredSelection()
        editView.requestFocus()
    }

    private fun appendAtCursor(txt: String) {
        blockIME = true
        val selStart = editView.selectionEnd
        editView.text.insert(selStart, txt)
        editView.setSelection(selStart + txt.length)
        blockIME = false
    }


}