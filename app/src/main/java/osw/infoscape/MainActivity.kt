package osw.infoscape

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.transition.Fade
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.core.DefaultTransitionFactory
import com.github.chuross.morirouter.core.MoriRouterOptions
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.infoscape.db.AppDatabase
import osw.infoscape.db.DbWorker
import osw.infoscape.fragment.SettingsFragment
import osw.infoscape.util.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object {
        const val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 150
    }

    lateinit var router: MoriRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        val transitionFactory = DefaultTransitionFactory { Fade() }
        val options = MoriRouterOptions.Builder(R.id.container)
            .setEnterTransitionFactory(transitionFactory)
            .setExitTransitionFactory(transitionFactory)
            .build()
        router = MoriRouter(supportFragmentManager, options)
        DbWorker.getInstance()!!.start()
        AsyncWorker.getInstance()!!.start()
        LuceneHelper.init(this)
//        checkSyncDir()
        checkPermissions()
        switchNightmode()
        FileHelper.initFilenames(this)
        if (PrefHelper.favStart(this)) {
            router.favouriteView().launch()
        } else {
            router.documentView("", "").launch()
        }
    }

    fun switchNightmode() {
        AppCompatDelegate.setDefaultNightMode(PrefHelper.getNightmode(this))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onDestroy() {
        DbWorker.getInstance()?.quit()
        DbWorker.destroyInstance()
        AsyncWorker.getInstance()?.quit()
        AsyncWorker.destroyInstance()
        AppDatabase.getInstance(this)?.close()
        AppDatabase.destroyInstance()
        LuceneHelper.close()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                openSettings()
                return true
            }
            R.id.action_reindex -> {
                AsyncWorker.postTask(this) {
                    LuceneHelper.recreateIndex(this)
                    DialogHelper.info(this, "Reindex", "New index created")
                }
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_documents -> router.documentView("", "").launch()
            R.id.nav_fav -> router.favouriteView().launch()
            R.id.nav_search -> router.searchView().launch()
            R.id.nav_csvlist -> router.documentView("", Consts.CSV_EXT).launch()
            R.id.nav_imglist -> router.documentView("", Consts.JPG_EXT).launch()
            R.id.nav_mdlist -> router.documentView("", Consts.MARKDOWN_EXT).launch()
            R.id.nav_pdflist -> router.documentView("", Consts.PDF_EXT).launch()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun openSettings() {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val settings = SettingsFragment()
        fragmentTransaction.replace(R.id.container, settings)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            ) {
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    DialogHelper.info(this, "Permission", "Permission granted")
                } else {
                    DialogHelper.info(this, "Permission", "Permission granted")
                }
                return
            }
            else -> {
                // Ignore all other requests.
            }
        }
    }


}
